"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
const bodyParser = __importStar(require("body-parser"));
const express_1 = __importDefault(require("express"));
const fs = __importStar(require("fs"));
const xero_node_1 = require("xero-node");
const helper_1 = __importDefault(require("./helper"));
const jwt_decode_1 = __importDefault(require("jwt-decode"));
const models_1 = require("xero-node/dist/gen/model/assets/models");
const models_2 = require("xero-node/dist/gen/model/projects/models");
const models_3 = require("xero-node/dist/gen/model/payroll-au/models");
const models_4 = require("xero-node/dist/gen/model/bankfeeds/models");
const models_5 = require("xero-node/dist/gen/model/payroll-uk/models");
const session = require("express-session");
var FileStore = require('session-file-store')(session);
const path = require("path");
const mime = require("mime-types");
const client_id = process.env.CLIENT_ID;
const client_secret = process.env.CLIENT_SECRET;
const redirectUrl = process.env.REDIRECT_URI;
const scopes = "offline_access openid profile email accounting.transactions accounting.transactions.read accounting.reports.read accounting.journals.read accounting.settings accounting.settings.read accounting.contacts accounting.contacts.read accounting.attachments accounting.attachments.read files files.read assets assets.read projects projects.read payroll.employees payroll.payruns payroll.payslip payroll.timesheets payroll.settings";
// bankfeeds
const xero = new xero_node_1.XeroClient({
    clientId: client_id,
    clientSecret: client_secret,
    redirectUris: [redirectUrl],
    scopes: scopes.split(" "),
});
if (!client_id || !client_secret || !redirectUrl) {
    throw Error('Environment Variables not all set - please check your .env file in the project root or create one!');
}
const sleep = (ms) => {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
};
class App {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.routes();
        this.app.set("views", path.join(__dirname, "views"));
        this.app.set("view engine", "ejs");
        this.app.use(express_1.default.static(path.join(__dirname, "public")));
        this.consentUrl = xero.buildConsentUrl();
    }
    config() {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }
    // helpers
    authenticationData(req, _res) {
        return {
            decodedIdToken: req.session.decodedIdToken,
            tokenSet: req.session.tokenSet,
            decodedAccessToken: req.session.decodedAccessToken,
            accessTokenExpires: this.timeSince(req.session.decodedAccessToken),
            allTenants: req.session.allTenants,
            activeTenant: req.session.activeTenant
        };
    }
    timeSince(token) {
        if (token) {
            const timestamp = token['exp'];
            const myDate = new Date(timestamp * 1000);
            return myDate.toLocaleString();
        }
        else {
            return '';
        }
    }
    routes() {
        const router = express_1.default.Router();
        router.get("/", (req, res) => __awaiter(this, void 0, void 0, function* () {
            if (req.session.tokenSet) {
                // This reset the session and required data on the xero client after ts recompile
                yield xero.setTokenSet(req.session.tokenSet);
                yield xero.updateTenants();
            }
            try {
                const authData = this.authenticationData(req, res);
                res.render("home", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: authData
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/callback", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // calling apiCallback will setup all the client with
                // and return the orgData of each authorized tenant
                const tokenSet = yield xero.apiCallback(req.url);
                yield xero.updateTenants();
                // this is where you can associate & save your
                // `tokenSet` to a user in your Database
                req.session.tokenSet = tokenSet;
                if (tokenSet.id_token) {
                    const decodedIdToken = jwt_decode_1.default(tokenSet.id_token);
                    req.session.decodedIdToken = decodedIdToken;
                }
                const decodedAccessToken = jwt_decode_1.default(tokenSet.access_token);
                req.session.decodedAccessToken = decodedAccessToken;
                req.session.tokenSet = tokenSet;
                req.session.allTenants = xero.tenants;
                req.session.activeTenant = xero.tenants[0];
                res.render("callback", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res)
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.post("/change_organisation", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const activeOrgId = req.body.active_org_id;
                const picked = xero.tenants.filter((tenant) => tenant.tenantId == activeOrgId)[0];
                req.session.activeTenant = picked;
                const authData = this.authenticationData(req, res);
                res.render("home", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res)
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/refresh-token", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const tokenSet = yield xero.readTokenSet();
                console.log('token expires in:', tokenSet.expires_in, 'seconds');
                console.log('tokenSet.expires_at:', tokenSet.expires_at, 'milliseconds');
                console.log('Readable expiration:', new Date(tokenSet.expires_at * 1000).toLocaleString());
                const now = new Date().getTime();
                if (tokenSet.expires_in > now) {
                    console.log('token is currently expired: ', tokenSet);
                }
                else {
                    console.log('tokenSet is not expired!');
                }
                // you can refresh the token using the fully initialized client levereging openid-client
                yield xero.refreshToken();
                // or if you already generated a tokenSet and have a valid (< 60 days refresh token),
                // you can initialize an empty client and refresh by passing the client, secret, and refresh_token
                const newXeroClient = new xero_node_1.XeroClient();
                const newTokenSet = yield newXeroClient.refreshWithRefreshToken(client_id, client_secret, tokenSet.refresh_token);
                const decodedIdToken = jwt_decode_1.default(newTokenSet.id_token);
                const decodedAccessToken = jwt_decode_1.default(newTokenSet.access_token);
                req.session.decodedIdToken = decodedIdToken;
                req.session.decodedAccessToken = decodedAccessToken;
                req.session.tokenSet = newTokenSet;
                req.session.allTenants = xero.tenants;
                req.session.activeTenant = xero.tenants[0];
                const authData = this.authenticationData(req, res);
                res.render("home", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res)
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/disconnect", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const updatedTokenSet = yield xero.disconnect(req.session.activeTenant.id);
                yield xero.updateTenants();
                if (xero.tenants.length > 0) {
                    const decodedIdToken = jwt_decode_1.default(updatedTokenSet.id_token);
                    const decodedAccessToken = jwt_decode_1.default(updatedTokenSet.access_token);
                    req.session.decodedIdToken = decodedIdToken;
                    req.session.decodedAccessToken = decodedAccessToken;
                    req.session.tokenSet = updatedTokenSet;
                    req.session.allTenants = xero.tenants;
                    req.session.activeTenant = xero.tenants[0];
                }
                else {
                    req.session.decodedIdToken = undefined;
                    req.session.decodedAccessToken = undefined;
                    req.session.allTenants = undefined;
                    req.session.activeTenant = undefined;
                }
                const authData = this.authenticationData(req, res);
                res.render("home", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: authData
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        // ******************************************************************************************************************** ACCOUNTING API
        router.get("/accounts", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // GET ALL
                const accountsGetResponse = yield xero.accountingApi.getAccounts(req.session.activeTenant.tenantId);
                // CREATE
                const account = { name: "Foo" + helper_1.default.getRandomNumber(1000000), code: "c:" + helper_1.default.getRandomNumber(1000000), type: xero_node_1.AccountType.EXPENSE, hasAttachments: true };
                const accountCreateResponse = yield xero.accountingApi.createAccount(req.session.activeTenant.tenantId, account);
                const accountId = accountCreateResponse.body.accounts[0].accountID;
                // GET ONE
                const accountGetResponse = yield xero.accountingApi.getAccount(req.session.activeTenant.tenantId, accountId);
                // UPDATE
                const accountUp = { name: "Bar" + helper_1.default.getRandomNumber(1000000) };
                const accounts = { accounts: [accountUp] };
                const accountUpdateResponse = yield xero.accountingApi.updateAccount(req.session.activeTenant.tenantId, accountId, accounts);
                // CREATE ATTACHMENT
                const filename = "xero-dev.png";
                const pathToUpload = path.resolve(__dirname, "../public/images/xero-dev.png");
                const readStream = fs.createReadStream(pathToUpload);
                const contentType = mime.lookup(filename);
                const accountAttachmentsResponse = yield xero.accountingApi.createAccountAttachmentByFileName(req.session.activeTenant.tenantId, accountId, filename, readStream, {
                    headers: {
                        'Content-Type': contentType
                    }
                });
                const attachment = accountAttachmentsResponse.body;
                const attachmentId = attachment.attachments[0].attachmentID;
                // GET ATTACHMENTS
                const accountAttachmentsGetResponse = yield xero.accountingApi.getAccountAttachments(req.session.activeTenant.tenantId, accountId);
                // GET ATTACHMENT BY ID
                const accountAttachmentsGetByIdResponse = yield xero.accountingApi.getAccountAttachmentById(req.session.activeTenant.tenantId, accountId, attachmentId, contentType);
                fs.writeFile(`img-temp-${filename}`, accountAttachmentsGetByIdResponse.body, (err) => {
                    if (err) {
                        throw err;
                    }
                    console.log("file written successfully");
                });
                // GET ATTACHMENT BY FILENAME
                const accountAttachmentsGetByFilenameResponse = yield xero.accountingApi.getAccountAttachmentByFileName(req.session.activeTenant.tenantId, accountId, filename, contentType);
                fs.writeFile(`img-temp-${filename}`, accountAttachmentsGetByFilenameResponse.body, (err) => {
                    if (err) {
                        throw err;
                    }
                    console.log("file written successfully");
                });
                // DELETE
                // let accountDeleteResponse = await xero.accountingApi.deleteAccount(req.session.activeTenant.tenantId, accountId);
                res.render("accounts", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    accountsCount: accountsGetResponse.body.accounts.length,
                    getOneName: accountGetResponse.body.accounts[0].name,
                    createName: accountCreateResponse.body.accounts[0].name,
                    updateName: accountUpdateResponse.body.accounts[0].name,
                    attachments: accountAttachmentsGetResponse.body,
                    deleteName: 'un-comment to DELETE'
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/banktransactions", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // GET ALL
                const bankTransactionsGetResponse = yield xero.accountingApi.getBankTransactions(req.session.activeTenant.tenantId);
                // CREATE ONE OR MORE BANK TRANSACTION
                const contactsResponse = yield xero.accountingApi.getContacts(req.session.activeTenant.tenantId);
                const useContact = { contactID: contactsResponse.body.contacts[0].contactID };
                const allAccounts = yield xero.accountingApi.getAccounts(req.session.activeTenant.tenantId);
                const validAccountCode = allAccounts.body.accounts.filter(e => !['NONE', 'BASEXCLUDED'].includes(e.taxType))[0].code;
                const lineItems = [{
                        description: "consulting",
                        quantity: 1.0,
                        unitAmount: 20.0,
                        accountCode: validAccountCode,
                    }];
                const where = 'Status=="' + xero_node_1.Account.StatusEnum.ACTIVE + '" AND Type=="' + xero_node_1.Account.BankAccountTypeEnum.BANK + '"';
                const accountsResponse = yield xero.accountingApi.getAccounts(req.session.activeTenant.tenantId, null, where);
                const useBankAccount = { accountID: accountsResponse.body.accounts[0].accountID };
                const newBankTransaction = {
                    type: xero_node_1.BankTransaction.TypeEnum.SPEND,
                    contact: useContact,
                    lineItems,
                    bankAccount: useBankAccount,
                    date: "2019-09-19T00:00:00",
                };
                // Add bank transaction objects to array
                const newBankTransactions = new xero_node_1.BankTransactions();
                newBankTransactions.bankTransactions = [newBankTransaction, newBankTransaction];
                const bankTransactionCreateResponse = yield xero.accountingApi.createBankTransactions(req.session.activeTenant.tenantId, newBankTransactions, false);
                // UPDATE OR CREATE ONE OR MORE BANK TRANSACTION
                const newBankTransaction2 = {
                    type: xero_node_1.BankTransaction.TypeEnum.SPEND,
                    contact: useContact,
                    lineItems,
                    bankAccount: useBankAccount,
                    date: "2019-09-19T00:00:00",
                };
                // Swap in this lineItem arry to force an ERROR with an invalid account code
                const lineItems2 = [{
                        description: "consulting",
                        quantity: 1.0,
                        unitAmount: 20.0,
                        accountCode: "6666666666",
                    }];
                const newBankTransaction3 = {
                    bankTransactionID: bankTransactionCreateResponse.body.bankTransactions[0].bankTransactionID,
                    type: xero_node_1.BankTransaction.TypeEnum.SPEND,
                    contact: useContact,
                    bankAccount: useBankAccount,
                    reference: "Changed",
                    lineItems: lineItems
                };
                const upBankTransactions = new xero_node_1.BankTransactions();
                upBankTransactions.bankTransactions = [newBankTransaction2, newBankTransaction3];
                const bankTransactionUpdateOrCreateResponse = yield xero.accountingApi.updateOrCreateBankTransactions(req.session.activeTenant.tenantId, upBankTransactions, false);
                // GET ONE
                const bankTransactionId = bankTransactionCreateResponse.body.bankTransactions[0].bankTransactionID;
                const bankTransactionGetResponse = yield xero.accountingApi.getBankTransaction(req.session.activeTenant.tenantId, bankTransactionId);
                // CREATE ATTACHMENT
                const filename = "xero-dev.png";
                const pathToUpload = path.resolve(__dirname, "../public/images/xero-dev.png");
                const readStream = fs.createReadStream(pathToUpload);
                const contentType = mime.lookup(filename);
                const bankTransactionAttachmentsResponse = yield xero.accountingApi.createBankTransactionAttachmentByFileName(req.session.activeTenant.tenantId, bankTransactionId, filename, readStream, {
                    headers: {
                        'Content-Type': contentType
                    }
                });
                // UPDATE status to deleted
                const bankTransactionUp = Object.assign({}, bankTransactionGetResponse.body.bankTransactions[0]);
                delete bankTransactionUp.updatedDateUTC;
                delete bankTransactionUp.contact; // also has an updatedDateUTC
                bankTransactionUp.status = xero_node_1.BankTransaction.StatusEnum.DELETED;
                const bankTransactions = { bankTransactions: [bankTransactionUp] };
                const bankTransactionUpdateResponse = yield xero.accountingApi.updateBankTransaction(req.session.activeTenant.tenantId, bankTransactionId, bankTransactions);
                res.render("banktransactions", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    bankTransactionsCount: bankTransactionsGetResponse.body.bankTransactions.length,
                    createID: bankTransactionCreateResponse.body.bankTransactions[0].bankTransactionID,
                    getOneStatus: bankTransactionGetResponse.body.bankTransactions[0].type,
                    updatedStatus: bankTransactionUpdateResponse.body.bankTransactions[0].status,
                });
            }
            catch (e) {
                console.error(e);
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/banktranfers", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // GET ALL
                const getBankTransfersResult = yield xero.accountingApi.getBankTransfers(req.session.activeTenant.tenantId);
                // FIRST we need two Accounts type=BANK
                const account1 = {
                    name: "Ima Bank: " + helper_1.default.getRandomNumber(1000000),
                    code: "" + helper_1.default.getRandomNumber(1000000),
                    type: xero_node_1.AccountType.BANK,
                    bankAccountNumber: helper_1.default.getRandomNumber(209087654321050).toString()
                };
                const account2 = {
                    name: "Ima Bank: " + helper_1.default.getRandomNumber(1000000),
                    code: "" + helper_1.default.getRandomNumber(1000000),
                    type: xero_node_1.AccountType.BANK,
                    bankAccountNumber: helper_1.default.getRandomNumber(209087654321051).toString(),
                };
                const created1 = yield xero.accountingApi.createAccount(req.session.activeTenant.tenantId, account1);
                const created2 = yield xero.accountingApi.createAccount(req.session.activeTenant.tenantId, account2);
                const acc1 = created1.body.accounts[0];
                const acc2 = created2.body.accounts[0];
                // CREATE
                const bankTransfer = {
                    fromBankAccount: {
                        accountID: acc1.accountID,
                        name: acc1.name
                    },
                    toBankAccount: {
                        accountID: acc2.accountID,
                        name: acc2.name
                    },
                    amount: 1000
                };
                const bankTransfers = { bankTransfers: [bankTransfer] };
                const createBankTransfer = yield xero.accountingApi.createBankTransfer(req.session.activeTenant.tenantId, bankTransfers);
                // GET ONE
                const getBankTransfer = yield xero.accountingApi.getBankTransfer(req.session.activeTenant.tenantId, createBankTransfer.body.bankTransfers[0].bankTransferID);
                res.render("banktranfers", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    allBankTransfers: getBankTransfersResult.body.bankTransfers,
                    createBankTransferId: createBankTransfer.body.bankTransfers[0].bankTransferID,
                    getBankTransferId: getBankTransfer.body.bankTransfers[0].bankTransferID
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/batchpayments", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const allContacts = yield xero.accountingApi.getContacts(req.session.activeTenant.tenantId);
                // Then create an approved/authorised invoice
                const invoice1 = {
                    type: xero_node_1.Invoice.TypeEnum.ACCREC,
                    contact: {
                        contactID: allContacts.body.contacts[0].contactID
                    },
                    date: "2009-05-27T00:00:00",
                    dueDate: "2009-06-06T00:00:00",
                    lineAmountTypes: xero_node_1.LineAmountTypes.Exclusive,
                    lineItems: [
                        {
                            description: "Consulting services",
                            taxType: "OUTPUT",
                            quantity: 20,
                            unitAmount: 100.00,
                            accountCode: "200"
                        }
                    ],
                    status: xero_node_1.Invoice.StatusEnum.AUTHORISED
                };
                const newInvoices = new xero_node_1.Invoices();
                newInvoices.invoices = [invoice1];
                const createdInvoice = yield xero.accountingApi.createInvoices(req.session.activeTenant.tenantId, newInvoices);
                const invoice = createdInvoice.body.invoices[0];
                const accountsGetResponse = yield xero.accountingApi.getAccounts(req.session.activeTenant.tenantId);
                // CREATE
                const payment1 = {
                    account: { code: "001" },
                    date: "2019-12-31",
                    amount: 500,
                    invoice: {
                        invoiceID: invoice.invoiceID // Not typed correctly
                    }
                };
                // BatchPayment 'reference'?: string; is not optional
                const payments = {
                    account: {
                        accountID: accountsGetResponse.body.accounts[0].accountID
                    },
                    reference: "ref",
                    date: "2018-08-01",
                    payments: [
                        payment1
                    ]
                };
                const batchPayments = {
                    batchPayments: [
                        payments
                    ]
                };
                const createBatchPayment = yield xero.accountingApi.createBatchPayment(req.session.activeTenant.tenantId, batchPayments);
                // GET
                const apiResponse = yield xero.accountingApi.getBatchPayments(req.session.activeTenant.tenantId);
                res.render("batchpayments", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    createBatchPayment: createBatchPayment.body.batchPayments[0].batchPaymentID,
                    count: apiResponse.body.batchPayments.length
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/brandingthemes", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // GET ALL
                const getBrandingThemesResponse = yield xero.accountingApi.getBrandingThemes(req.session.activeTenant.tenantId);
                // GET ONE
                const getBrandingThemeResponse = yield xero.accountingApi.getBrandingTheme(req.session.activeTenant.tenantId, getBrandingThemesResponse.body.brandingThemes[0].brandingThemeID);
                // CREATE BRANDING THEME PAYMENT SERVICE
                // first we'll need a payment service - this will require a restricted scope 'paymentservices' - please contact api@xero.com to get access
                // const paymentServices: PaymentServices = { paymentServices: [{ paymentServiceName: `PayUpNow ${Helper.getRandomNumber(1000)}`, paymentServiceUrl: "https://www.payupnow.com/?invoiceNo=[INVOICENUMBER]&currency=[CURRENCY]&amount=[AMOUNTDUE]&shortCode=[SHORTCODE]", payNowText: "Time To Pay" }] };
                // const createPaymentServiceResponse = await xero.accountingApi.createPaymentService(req.session.activeTenant.tenantId, paymentServices);
                // const createBrandingThemePaymentServicesResponse = await xero.accountingApi.createBrandingThemePaymentServices(req.session.activeTenant.tenantId, getBrandingThemeResponse.body.brandingThemes[0].brandingThemeID, { paymentServiceID: createPaymentServiceResponse.body.paymentServices[0].paymentServiceID });
                // GET BRANDING THEME PAYMENT SERVICES
                // const getBrandingThemePaymentServicesResponse = await xero.accountingApi.getBrandingThemePaymentServices(req.session.activeTenant.tenantId, getBrandingThemeResponse.body.brandingThemes[0].brandingThemeID);
                res.render("brandingthemes", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    brandingThemesCount: getBrandingThemesResponse.body.brandingThemes.length,
                    brandingTheme: getBrandingThemeResponse.body.brandingThemes[0].name,
                    createBrandingThemePaymentService: 'createBrandingThemePaymentServicesResponse.body.paymentServices[0].paymentServiceID',
                    getBrandingThemePaymentService: 'getBrandingThemePaymentServicesResponse.body.paymentServices[0].paymentServiceName'
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/contacts", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // GET ALL
                const contactsGetResponse = yield xero.accountingApi.getContacts(req.session.activeTenant.tenantId);
                // CREATE ONE OR MORE
                const contact1 = { name: "Rick James: " + helper_1.default.getRandomNumber(1000000), firstName: "Rick", lastName: "James", emailAddress: "test@example.com" };
                const newContacts = new xero_node_1.Contacts();
                newContacts.contacts = [contact1];
                const contactCreateResponse = yield xero.accountingApi.createContacts(req.session.activeTenant.tenantId, newContacts);
                const contactId = contactCreateResponse.body.contacts[0].contactID;
                // UPDATE or CREATE BATCH - force validation error
                const person = {
                    firstName: 'Joe',
                    lastName: 'Schmo'
                };
                const updateContacts = new xero_node_1.Contacts();
                const contact2 = {
                    contactID: contactId,
                    name: "Rick James: " + helper_1.default.getRandomNumber(1000000),
                    firstName: "Rick",
                    lastName: "James",
                    emailAddress: "test@example.com",
                    contactPersons: [person]
                };
                const contact3 = { name: "Rick James: " + helper_1.default.getRandomNumber(1000000), firstName: "Rick", lastName: "James", emailAddress: "test@example.com" };
                updateContacts.contacts = [contact2, contact3];
                yield xero.accountingApi.updateOrCreateContacts(req.session.activeTenant.tenantId, updateContacts, false);
                // GET ONE
                const contactGetResponse = yield xero.accountingApi.getContact(req.session.activeTenant.tenantId, contactId);
                // UPDATE SINGLE
                const contactUpdate = { name: "Rick James Updated: " + helper_1.default.getRandomNumber(1000000) };
                const contacts = { contacts: [contactUpdate] };
                const contactUpdateResponse = yield xero.accountingApi.updateContact(req.session.activeTenant.tenantId, contactId, contacts);
                res.render("contacts", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    contactsCount: contactsGetResponse.body.contacts.length,
                    createName: contactCreateResponse.body.contacts[0].name,
                    getOneName: contactGetResponse.body.contacts[0].name,
                    updatedContact: contactUpdateResponse.body.contacts[0],
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/contactgroups", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // CREATE
                const contactGroupParams = { contactGroups: [{ name: 'Ima Contact Group' + helper_1.default.getRandomNumber(1000000) }] };
                const createContactGroup = yield xero.accountingApi.createContactGroup(req.session.activeTenant.tenantId, contactGroupParams);
                const contactGroup = createContactGroup.body.contactGroups[0];
                // GET
                const getContactGroup = yield xero.accountingApi.getContactGroup(req.session.activeTenant.tenantId, contactGroup.contactGroupID);
                // UPDATE
                const num = helper_1.default.getRandomNumber(1000000);
                const contact1 = { name: "Rick James: " + num, firstName: "Rick", lastName: "James", emailAddress: `foo+${num}@example.com` };
                const newContacts = new xero_node_1.Contacts();
                newContacts.contacts = [contact1];
                const contactCreateResponse = yield xero.accountingApi.createContacts(req.session.activeTenant.tenantId, newContacts);
                const createdContact = contactCreateResponse.body.contacts[0];
                const updatedContactGroupParams = {
                    contacts: [{ contactID: createdContact.contactID }]
                };
                // To create contacts w/in contact group you cannot pass a whole Contact
                // need to pass it as an aray of with the following key `{ contacts: [{ contactID: createdContact.contactID }] }`
                const updatedContactGroup = yield xero.accountingApi.createContactGroupContacts(req.session.activeTenant.tenantId, contactGroup.contactGroupID, updatedContactGroupParams);
                // DELETE
                const deletedContactGroupContact = yield xero.accountingApi.deleteContactGroupContact(req.session.activeTenant.tenantId, contactGroup.contactGroupID, createdContact.contactID);
                const deleted = deletedContactGroupContact.response.statusCode === 204;
                // GET ALL
                const allContactGroups = yield xero.accountingApi.getContactGroups(req.session.activeTenant.tenantId);
                res.render("contactgroups", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    createdContactGroupID: contactGroup.contactGroupID,
                    getContactGroupName: getContactGroup.body.contactGroups[0].name,
                    updatedContactGroupContactID: updatedContactGroup.body.contacts[0].contactID,
                    deletedContactGroupContact: deleted ? `${createdContact.contactID} removed from contact group` : 'failed to delete',
                    count: allContactGroups.body.contactGroups.length
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/creditnotes", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                const getCreditNotesResponse = yield xero.accountingApi.getCreditNotes(req.session.activeTenant.tenantId);
                // we're going to need a contact
                const contactsGetResponse = yield xero.accountingApi.getContacts(req.session.activeTenant.tenantId);
                // and an invoice for that contact
                const invoices = {
                    invoices: [
                        {
                            type: xero_node_1.Invoice.TypeEnum.ACCREC,
                            contact: {
                                contactID: contactsGetResponse.body.contacts[0].contactID
                            },
                            expectedPaymentDate: "2029-10-20T00:00:00",
                            invoiceNumber: `XERO:${helper_1.default.getRandomNumber(1000000)}`,
                            reference: `REF:${helper_1.default.getRandomNumber(1000000)}`,
                            url: "https://deeplink-to-your-site.com",
                            currencyCode: req.session.activeTenant.baseCurrency,
                            status: xero_node_1.Invoice.StatusEnum.AUTHORISED,
                            lineAmountTypes: xero_node_1.LineAmountTypes.Exclusive,
                            date: "2029-05-27T00:00:00",
                            dueDate: "2029-06-06T00:00:00",
                            lineItems: [
                                {
                                    description: "MacBook - White",
                                    quantity: 1.0000,
                                    unitAmount: 1995.00,
                                    accountCode: "720"
                                }
                            ]
                        }
                    ]
                };
                const createInvoiceResponse = yield xero.accountingApi.createInvoices(req.session.activeTenant.tenantId, invoices);
                // CREATE
                const newCreditNotes = {
                    creditNotes: [
                        {
                            type: xero_node_1.CreditNote.TypeEnum.ACCRECCREDIT,
                            status: xero_node_1.CreditNote.StatusEnum.DRAFT,
                            contact: {
                                contactID: contactsGetResponse.body.contacts[0].contactID
                            },
                            date: "2020-04-06",
                            lineAmountTypes: xero_node_1.LineAmountTypes.Exclusive,
                            lineItems: [
                                {
                                    description: "MacBook - White",
                                    quantity: 1.0000,
                                    unitAmount: 1995.00,
                                    accountCode: "720"
                                }
                            ]
                        }
                    ]
                };
                const createCreditNotesResponse = yield xero.accountingApi.createCreditNotes(req.session.activeTenant.tenantId, newCreditNotes);
                // UPDATE
                newCreditNotes.creditNotes[0].status = xero_node_1.CreditNote.StatusEnum.AUTHORISED;
                const updateCreditNoteResponse = yield xero.accountingApi.updateCreditNote(req.session.activeTenant.tenantId, createCreditNotesResponse.body.creditNotes[0].creditNoteID, newCreditNotes);
                // CREATE CREDIT NOTE HISTORY
                const historyRecords = {
                    historyRecords: [
                        {
                            details: "This is a history record " + helper_1.default.getRandomNumber(1000)
                        }
                    ]
                };
                const createCreditNoteHistoryResponse = yield xero.accountingApi.createCreditNoteHistory(req.session.activeTenant.tenantId, createCreditNotesResponse.body.creditNotes[0].creditNoteID, historyRecords);
                // CREATE CREDIT NOTE ALLOCATION
                const allocations = {
                    allocations: [
                        {
                            date: "2020-04-08",
                            amount: 3.50,
                            invoice: {
                                invoiceID: createInvoiceResponse.body.invoices[0].invoiceID
                            }
                        }
                    ]
                };
                const createCreditNoteAllocationResponse = yield xero.accountingApi.createCreditNoteAllocation(req.session.activeTenant.tenantId, createCreditNotesResponse.body.creditNotes[0].creditNoteID, allocations);
                const filename = "xero-dev.png";
                const pathToUpload = path.resolve(__dirname, "../public/images/xero-dev.png");
                const readStream = fs.createReadStream(pathToUpload);
                const contentType = mime.lookup(filename);
                // CREATE CREDIT NOTE ATTACHMENT BY FILE NAME
                const createCreditNoteAttachmentByFileNameResponse = yield xero.accountingApi.createCreditNoteAttachmentByFileName(req.session.activeTenant.tenantId, createCreditNotesResponse.body.creditNotes[0].creditNoteID, filename, readStream, true, { headers: { 'Content-Type': contentType } });
                // UPDATE CREDIT NOTE ATTACHMENT BY FILE NAME
                // const updateCreditNoteAttachmentByFileNameResponse = await xero.accountingApi.updateCreditNoteAttachmentByFileName(
                //   req.session.activeTenant.tenantId,
                //   createCreditNotesResponse.body.creditNotes[0].creditNoteID,
                //   filename,
                //   readStream
                // );
                // GET CREDIT NOTE
                const getCreditNoteResponse = yield xero.accountingApi.getCreditNote(req.session.activeTenant.tenantId, createCreditNotesResponse.body.creditNotes[0].creditNoteID);
                // GET CREDIT NOTE HISTORY
                const getCreditNoteHistoryResponse = yield xero.accountingApi.getCreditNoteHistory(req.session.activeTenant.tenantId, createCreditNotesResponse.body.creditNotes[0].creditNoteID);
                // GET CREDIT NOTE ATTACHMENTS
                const getCreditNoteAttachmentsResponse = yield xero.accountingApi.getCreditNoteAttachments(req.session.activeTenant.tenantId, createCreditNotesResponse.body.creditNotes[0].creditNoteID);
                // GET CREDIT NOTE ATTACHMENT BY ID
                const getCreditNoteAttachmentByIdResponse = yield xero.accountingApi.getCreditNoteAttachmentById(req.session.activeTenant.tenantId, createCreditNotesResponse.body.creditNotes[0].creditNoteID, getCreditNoteAttachmentsResponse.body.attachments[0].attachmentID, contentType);
                // GET CREDIT NOTE ATTACHMENT BY FILE NAME
                const getCreditNoteAttachmentByFileNameResponse = yield xero.accountingApi.getCreditNoteAttachmentByFileName(req.session.activeTenant.tenantId, createCreditNotesResponse.body.creditNotes[0].creditNoteID, getCreditNoteAttachmentsResponse.body.attachments[0].fileName, contentType);
                // GET CREDIT NOTE AS PDF
                const getCreditNoteAsPdfResponse = yield xero.accountingApi.getCreditNoteAsPdf(req.session.activeTenant.tenantId, createCreditNotesResponse.body.creditNotes[0].creditNoteID);
                res.render("creditnotes", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: getCreditNotesResponse.body.creditNotes.length,
                    create: createCreditNotesResponse.body.creditNotes[0].creditNoteID,
                    update: updateCreditNoteResponse.body.creditNotes[0].status,
                    createHistoryRecord: createCreditNoteHistoryResponse.body.historyRecords[0].details,
                    createAllocation: createCreditNoteAllocationResponse.body.allocations[0].amount,
                    createAttachmentByFileName: createCreditNoteAttachmentByFileNameResponse.body.attachments[0].attachmentID,
                    getOne: getCreditNoteResponse.body.creditNotes[0].contact.contactID,
                    historyRecords: getCreditNoteHistoryResponse.body.historyRecords.length,
                    attachmentsCount: getCreditNoteAttachmentsResponse.body.attachments.length,
                    attachmentByID: getCreditNoteAttachmentByIdResponse.body,
                    attachmentByFilName: getCreditNoteAttachmentByFileNameResponse.body,
                    attachmentAsPDF: getCreditNoteAsPdfResponse.body
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/currencies", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                const apiResponse = yield xero.accountingApi.getCurrencies(req.session.activeTenant.tenantId);
                // CREATE - only works once per currency code
                // const newCurrency: Currency = {
                //   code: CurrencyCode.GBP,
                // };
                // const createCurrencyResponse = await xero.accountingApi.createCurrency(req.session.activeTenant.tenantId, newCurrency);
                res.render("currencies", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    currencies: apiResponse.body.currencies
                    // newCurrency: createCurrencyResponse.body.currencies[0].description
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/employees", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                const getEmployeesResponse = yield xero.accountingApi.getEmployees(req.session.activeTenant.tenantId);
                // CREATE
                const firstName = "First" + helper_1.default.getRandomNumber(1000);
                const lastName = "Last" + helper_1.default.getRandomNumber(1000);
                const employees = {
                    employees: [
                        {
                            firstName: firstName,
                            lastName: firstName
                        }
                    ]
                };
                const createEmployeesResponse = yield xero.accountingApi.createEmployees(req.session.activeTenant.tenantId, employees);
                // GET ONE
                const getEmployeeResponse = yield xero.accountingApi.getEmployee(req.session.activeTenant.tenantId, createEmployeesResponse.body.employees[0].employeeID);
                // UPDATE
                const updatedEmployees = {
                    employees: [{
                            firstName: firstName,
                            lastName: firstName,
                            externalLink: {
                                url: "http://twitter.com/#!/search/First+Last"
                            }
                        }]
                };
                const updateEmployeeResponse = yield xero.accountingApi.updateOrCreateEmployees(req.session.activeTenant.tenantId, updatedEmployees);
                res.render("employees", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: getEmployeesResponse.body.employees.length,
                    createdEmployeeId: createEmployeesResponse.body.employees[0].employeeID,
                    getEmployeeName: getEmployeeResponse.body.employees[0].firstName,
                    updatedEmployeeId: updateEmployeeResponse.body.employees[0].employeeID
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/invoicereminders", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                const apiResponse = yield xero.accountingApi.getInvoiceReminders(req.session.activeTenant.tenantId);
                res.render("invoicereminders", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: apiResponse.body.invoiceReminders.length,
                    enabled: apiResponse.body.invoiceReminders[0].enabled
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/invoices", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const brandingTheme = yield xero.accountingApi.getBrandingThemes(req.session.activeTenant.tenantId);
                const num = helper_1.default.getRandomNumber(1000000);
                const contact1 = { name: "Test User: " + num, firstName: "Rick", lastName: "James", emailAddress: req.session.decodedIdToken.email };
                const newContacts = new xero_node_1.Contacts();
                newContacts.contacts = [contact1];
                yield xero.accountingApi.createContacts(req.session.activeTenant.tenantId, newContacts);
                const contactsResponse = yield xero.accountingApi.getContacts(req.session.activeTenant.tenantId);
                const selfContact = contactsResponse.body.contacts.filter(contact => contact.emailAddress === req.session.decodedIdToken.email);
                const where = 'Status=="' + xero_node_1.Account.StatusEnum.ACTIVE + '" AND Type=="' + xero_node_1.AccountType.EXPENSE + '"';
                const getAccountsResponse = yield xero.accountingApi.getAccounts(req.session.activeTenant.tenantId, null, where);
                const invoice1 = {
                    type: xero_node_1.Invoice.TypeEnum.ACCREC,
                    contact: {
                        contactID: selfContact[0].contactID
                    },
                    expectedPaymentDate: "2009-10-20T00:00:00",
                    invoiceNumber: `XERO:${helper_1.default.getRandomNumber(1000000)}`,
                    reference: `REF:${helper_1.default.getRandomNumber(1000000)}`,
                    brandingThemeID: brandingTheme.body.brandingThemes[0].brandingThemeID,
                    url: "https://deeplink-to-your-site.com",
                    hasAttachments: true,
                    currencyCode: req.session.activeTenant.baseCurrency,
                    status: xero_node_1.Invoice.StatusEnum.SUBMITTED,
                    lineAmountTypes: xero_node_1.LineAmountTypes.Inclusive,
                    subTotal: 87.11,
                    totalTax: 10.89,
                    total: 98.00,
                    date: "2009-05-27T00:00:00",
                    dueDate: "2009-06-06T00:00:00",
                    lineItems: [
                        {
                            description: "Consulting services",
                            taxType: "NONE",
                            quantity: 20,
                            unitAmount: 100.00,
                            accountCode: getAccountsResponse.body.accounts[0].code
                        },
                        {
                            description: "Mega Consulting services",
                            taxType: "NONE",
                            quantity: 10,
                            unitAmount: 500.00,
                            accountCode: getAccountsResponse.body.accounts[0].code
                        }
                    ]
                };
                // Array of Invoices needed
                const newInvoices = new xero_node_1.Invoices();
                newInvoices.invoices = [invoice1, invoice1];
                // CREATE ONE OR MORE INVOICES
                const createdInvoice = yield xero.accountingApi.createInvoices(req.session.activeTenant.tenantId, newInvoices, false);
                // Since we are using summarizeErrors = false we get 200 OK statuscode
                // Our array of created invoices include those that succeeded and those with validation errors.
                // loop over the invoices and if it has an error, loop over the error messages
                for (let i = 0; i < createdInvoice.body.invoices.length; i++) {
                    if (createdInvoice.body.invoices[i].hasErrors) {
                        let errors = createdInvoice.body.invoices[i].validationErrors;
                        for (let j = 0; j < errors.length; j++) {
                            console.log(errors[j].message);
                        }
                    }
                }
                // CREATE ONE OR MORE INVOICES - FORCE Validation error with bad account code
                const updateInvoices = new xero_node_1.Invoices();
                const invoice2 = {
                    type: xero_node_1.Invoice.TypeEnum.ACCREC,
                    contact: {
                        contactID: selfContact[0].contactID
                    },
                    status: xero_node_1.Invoice.StatusEnum.SUBMITTED,
                    date: "2009-05-27T00:00:00",
                    dueDate: "2009-06-06T00:00:00",
                    lineItems: [
                        {
                            description: "Consulting services",
                            taxType: "NONE",
                            quantity: 20,
                            unitAmount: 100.00,
                            accountCode: "99999999"
                        }
                    ]
                };
                updateInvoices.invoices = [invoice1, invoice2];
                yield xero.accountingApi.updateOrCreateInvoices(req.session.activeTenant.tenantId, updateInvoices, false);
                // GET ONE
                const getInvoice = yield xero.accountingApi.getInvoice(req.session.activeTenant.tenantId, createdInvoice.body.invoices[0].invoiceID);
                const invoiceId = getInvoice.body.invoices[0].invoiceID;
                // UPDATE
                const newReference = { reference: `NEW-REF:${helper_1.default.getRandomNumber(1000000)}` };
                const invoiceToUpdate = {
                    invoices: [
                        Object.assign(invoice1, newReference)
                    ]
                };
                const updatedInvoices = yield xero.accountingApi.updateInvoice(req.session.activeTenant.tenantId, invoiceId, invoiceToUpdate);
                // GET ALL
                const totalInvoices = yield xero.accountingApi.getInvoices(req.session.activeTenant.tenantId);
                res.render("invoices", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    invoiceId,
                    email: req.session.decodedIdToken.email,
                    createdInvoice: createdInvoice.body.invoices[0],
                    updatedInvoice: updatedInvoices.body.invoices[0],
                    count: totalInvoices.body.invoices.length
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/invoice-as-pdf", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // GET ALL
                const totalInvoices = yield xero.accountingApi.getInvoices(req.session.activeTenant.tenantId);
                // GET one as PDF
                const getAsPdf = yield xero.accountingApi.getInvoiceAsPdf(req.session.activeTenant.tenantId, totalInvoices.body.invoices[0].invoiceID, { headers: { accept: 'application/pdf' } });
                res.setHeader('Content-Disposition', 'attachment; filename=invoice-as-pdf.pdf');
                res.contentType("application/pdf");
                res.send(getAsPdf.body);
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/email-invoice", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const invoiceID = req.query.invoiceID;
                // SEND Email
                const apiResponse = yield xero.accountingApi.emailInvoice(req.session.activeTenant.tenantId, invoiceID, {});
                res.render("invoices", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: apiResponse
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/invoices-filtered", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const filteredInvoices = yield xero.accountingApi.getInvoices(req.session.activeTenant.tenantId, new Date(2018), 'Type=="ACCREC"', 'reference DESC', undefined, undefined, undefined, ['PAID', 'DRAFT'], 0, true, false, 4, {
                    headers: {
                        'contentType': 'application/json'
                    }
                });
                res.render("invoices-filtered", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    filteredInvoices: filteredInvoices.body.invoices
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/attachment-invoice", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const totalInvoices = yield xero.accountingApi.getInvoices(req.session.activeTenant.tenantId, undefined, undefined, undefined, undefined, undefined, undefined, ['PAID']);
                // Attachments need to be uploaded to associated objects https://developer.xero.com/documentation/api/attachments
                // CREATE ATTACHMENT
                const filename = "xero-dev.png";
                const pathToUpload = path.resolve(__dirname, "../public/images/xero-dev.png");
                const readStream = fs.createReadStream(pathToUpload);
                const contentType = mime.lookup(filename);
                const fileAttached = yield xero.accountingApi.createInvoiceAttachmentByFileName(req.session.activeTenant.tenantId, totalInvoices.body.invoices[0].invoiceID, filename, readStream, true, {
                    headers: {
                        "Content-Type": contentType,
                    },
                });
                res.render("attachment-invoice", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    attachments: fileAttached.body
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/items", (req, res) => __awaiter(this, void 0, void 0, function* () {
            // currently works with DEMO COMPANY specific data.. Will need to create proper accounts
            // w/ cOGS codes to have this work with an empty Xero Org
            try {
                // GET ALL
                const itemsGetResponse = yield xero.accountingApi.getItems(req.session.activeTenant.tenantId);
                // CREATE ONE or MORE ITEMS
                const item1 = {
                    code: "Foo" + helper_1.default.getRandomNumber(1000000),
                    name: "Bar",
                    purchaseDetails: {
                        unitPrice: 375.5000,
                        taxType: "NONE",
                        accountCode: "500"
                    },
                    salesDetails: {
                        unitPrice: 520.9900,
                        taxType: "NONE",
                        accountCode: "400",
                    }
                };
                const newItems = new xero_node_1.Items();
                newItems.items = [item1];
                const itemCreateResponse = yield xero.accountingApi.createItems(req.session.activeTenant.tenantId, newItems);
                const itemId = itemCreateResponse.body.items[0].itemID;
                // UPDATE OR CREATE ONE or MORE ITEMS - FORCE validation error on update
                item1.name = "Bar" + helper_1.default.getRandomNumber(1000000);
                const updateItems = new xero_node_1.Items();
                updateItems.items = [item1];
                yield xero.accountingApi.updateOrCreateItems(req.session.activeTenant.tenantId, updateItems, false);
                // GET ONE
                const itemGetResponse = yield xero.accountingApi.getItem(req.session.activeTenant.tenantId, itemsGetResponse.body.items[0].itemID);
                // UPDATE
                const itemUpdate = { code: "Foo" + helper_1.default.getRandomNumber(1000000), name: "Bar - updated", inventoryAssetAccountCode: item1.inventoryAssetAccountCode };
                const items = { items: [itemUpdate] };
                const itemUpdateResponse = yield xero.accountingApi.updateItem(req.session.activeTenant.tenantId, itemId, items);
                // DELETE
                const itemDeleteResponse = yield xero.accountingApi.deleteItem(req.session.activeTenant.tenantId, itemId);
                res.render("items", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    itemsCount: itemsGetResponse.body.items.length,
                    createName: itemCreateResponse.body.items[0].name,
                    getOneName: itemGetResponse.body.items[0].name,
                    updateName: itemUpdateResponse.body.items[0].name,
                    deleteResponse: itemDeleteResponse.response.statusCode
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/journals", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                const apiResponse = yield xero.accountingApi.getJournals(req.session.activeTenant.tenantId);
                res.render("journals", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    journals: apiResponse.body.journals
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/linked-transactions", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                const getLinkedTransactionsResponse = yield xero.accountingApi.getLinkedTransactions(req.session.activeTenant.tenantId);
                // CREATE
                // we need a source invoice, a target invoice, accounts, and contacts
                const where = 'Status=="' + xero_node_1.Account.StatusEnum.ACTIVE + '" AND Type=="' + xero_node_1.AccountType.EXPENSE + '"';
                const getAccountsResponse = yield xero.accountingApi.getAccounts(req.session.activeTenant.tenantId, null, where);
                const getContactsResponse = yield xero.accountingApi.getContacts(req.session.activeTenant.tenantId);
                const invoices = {
                    invoices: [
                        {
                            type: xero_node_1.Invoice.TypeEnum.ACCPAY,
                            contact: {
                                contactID: getContactsResponse.body.contacts[0].contactID
                            },
                            lineItems: [
                                {
                                    description: "source invoice line item description",
                                    quantity: 10,
                                    unitAmount: 3.50,
                                    taxType: "NONE",
                                    accountCode: getAccountsResponse.body.accounts[0].code
                                }
                            ],
                            dueDate: "2025-03-27",
                            status: xero_node_1.Invoice.StatusEnum.AUTHORISED
                        },
                        {
                            type: xero_node_1.Invoice.TypeEnum.ACCREC,
                            contact: {
                                contactID: getContactsResponse.body.contacts[1].contactID
                            },
                            lineItems: [
                                {
                                    description: "target invoice line item description",
                                    quantity: 15,
                                    unitAmount: 5.30,
                                    taxType: "NONE",
                                    accountCode: getAccountsResponse.body.accounts[0].code
                                }
                            ],
                            dueDate: "2025-03-27",
                            status: xero_node_1.Invoice.StatusEnum.AUTHORISED
                        }
                    ]
                };
                const createInvoicesResponse = yield xero.accountingApi.createInvoices(req.session.activeTenant.tenantId, invoices);
                const newLinkedTransaction = {
                    sourceTransactionID: createInvoicesResponse.body.invoices[0].invoiceID,
                    sourceLineItemID: createInvoicesResponse.body.invoices[0].lineItems[0].lineItemID
                };
                const createLinkedTransactionResponse = yield xero.accountingApi.createLinkedTransaction(req.session.activeTenant.tenantId, newLinkedTransaction);
                // GET ONE
                const getLinkedTransactionResponse = yield xero.accountingApi.getLinkedTransaction(req.session.activeTenant.tenantId, createLinkedTransactionResponse.body.linkedTransactions[0].linkedTransactionID);
                // UPDATE
                const updateLinkedTransactions = {
                    linkedTransactions: [
                        {
                            linkedTransactionID: createLinkedTransactionResponse.body.linkedTransactions[0].linkedTransactionID,
                            contactID: createInvoicesResponse.body.invoices[1].contact.contactID,
                            targetTransactionID: createInvoicesResponse.body.invoices[1].invoiceID,
                            targetLineItemID: createInvoicesResponse.body.invoices[1].lineItems[0].lineItemID
                        }
                    ]
                };
                const updateLinkedTransactionResponse = yield xero.accountingApi.updateLinkedTransaction(req.session.activeTenant.tenantId, createLinkedTransactionResponse.body.linkedTransactions[0].linkedTransactionID, updateLinkedTransactions);
                // DELETE
                const deleteLinkedTransactionResponse = yield xero.accountingApi.deleteLinkedTransaction(req.session.activeTenant.tenantId, createLinkedTransactionResponse.body.linkedTransactions[0].linkedTransactionID);
                res.render("linked-transactions", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: getLinkedTransactionsResponse.body.linkedTransactions.length,
                    create: createLinkedTransactionResponse.body.linkedTransactions[0].linkedTransactionID,
                    get: getLinkedTransactionResponse.body.linkedTransactions[0],
                    update: updateLinkedTransactionResponse.body.linkedTransactions[0],
                    deleted: deleteLinkedTransactionResponse.response.statusCode
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/manualjournals", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                const getManualJournalsResponse = yield xero.accountingApi.getManualJournals(req.session.activeTenant.tenantId);
                // CREATE
                const manualJournals = {
                    manualJournals: [
                        {
                            date: "2020-03-13",
                            status: xero_node_1.ManualJournal.StatusEnum.DRAFT,
                            narration: "Accrued expenses - prepaid insurance adjustment for March 2020",
                            journalLines: [
                                {
                                    lineAmount: 55.00,
                                    accountCode: "433"
                                },
                                {
                                    lineAmount: -55.00,
                                    accountCode: "620"
                                }
                            ]
                        }
                    ]
                };
                const createManualJournalResponse = yield xero.accountingApi.createManualJournals(req.session.activeTenant.tenantId, manualJournals);
                // CREATE MANUAL JOARNAL ATTACHMENT BY FILENAME
                const fileName = "xero-dev.png"; // {String} The name of the file being attached to a ManualJournal 
                const path = require("path");
                const mime = require("mime-types");
                const pathToUpload = path.resolve(__dirname, "../public/images/xero-dev.png"); // determine the path to your file
                // You'll need to add the import below to read your file
                // import * as fs from "fs";
                const body = fs.createReadStream(pathToUpload); // {fs.ReadStream} read the file
                const contentType = mime.lookup(fileName);
                const journalId = createManualJournalResponse.body.manualJournals[0].manualJournalID;
                const createManualJournalAttachmentByFileNameResponse = yield xero.accountingApi.createManualJournalAttachmentByFileName(req.session.activeTenant.tenantId, journalId, fileName, body, {
                    headers: {
                        "Content-Type": contentType,
                    }
                });
                // GET ONE
                const getManualJournalResponse = yield xero.accountingApi.getManualJournal(req.session.activeTenant.tenantId, journalId);
                // GET MANUAL JOURNAL ATTACHMENTS
                const getManualJournalAttachmentsResponse = yield xero.accountingApi.getManualJournalAttachments(req.session.activeTenant.tenantId, journalId);
                // GET MANUAL JOURNAL ATTACHMENT BY FILENAME
                const getManualJournalAttachmentByFileNameResponse = yield xero.accountingApi.getManualJournalAttachmentByFileName(req.session.activeTenant.tenantId, journalId, fileName, contentType);
                // GET MANUAL JOURNAL ATTACHMENT BY ID
                const getManualJournalAttachmentByIdResponse = yield xero.accountingApi.getManualJournalAttachmentById(req.session.activeTenant.tenantId, journalId, getManualJournalResponse.body.manualJournals[0].attachments[0].attachmentID, contentType);
                manualJournals.manualJournals[0].journalLines[0].description = "edited";
                // UPDATE MANUAL JOURNAL
                const updateManualJournalResponse = yield xero.accountingApi.updateManualJournal(req.session.activeTenant.tenantId, journalId, manualJournals);
                // UPDATE MANUAL JOURNAL ATTACHMENT BY FILENAME
                // const updateManualJournalAttachmentByFileNameResponse = await xero.accountingApi.updateManualJournalAttachmentByFileName(req.session.activeTenant.tenantId, journalId, fileName, body);
                res.render("manualjournals", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: getManualJournalsResponse.body.manualJournals.length,
                    create: createManualJournalResponse.body.manualJournals[0].manualJournalID,
                    mjAttachmentByFileName: createManualJournalAttachmentByFileNameResponse.body,
                    getMJ: getManualJournalResponse.body.manualJournals[0].narration,
                    getMJAttachments: getManualJournalAttachmentsResponse.body,
                    getMJAttachmentByFileName: getManualJournalAttachmentByFileNameResponse.body,
                    getMJAttachmentById: getManualJournalAttachmentByIdResponse.body,
                    updateMJ: updateManualJournalResponse.body.manualJournals[0].journalLines[0].description,
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/organisations", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                const getOrganizationsResponse = yield xero.accountingApi.getOrganisations(req.session.activeTenant.tenantId);
                // GET ORG CIS SETTINGS - UK only
                // const getOrgCISSettingsResponse = await xero.accountingApi.getOrganisationCISSettings(req.session.activeTenant.tenantId, getOrganizationsResponse.body.organisations[0].organisationID);
                res.render("organisations", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    orgs: getOrganizationsResponse.body.organisations
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/overpayments", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                const getOverpaymentsResponse = yield xero.accountingApi.getOverpayments(req.session.activeTenant.tenantId);
                // CREATE ALLOCATION
                // for that we'll need a contact
                const getContactsResponse = yield xero.accountingApi.getContacts(req.session.activeTenant.tenantId);
                const invoices = {
                    invoices: [
                        {
                            type: xero_node_1.Invoice.TypeEnum.ACCPAY,
                            contact: {
                                contactID: getContactsResponse.body.contacts[0].contactID
                            },
                            lineItems: [
                                {
                                    description: "Acme Tires",
                                    quantity: 2.0,
                                    unitAmount: 20.0,
                                    accountCode: "200",
                                    taxType: "OUTPUT",
                                    lineAmount: 40.0
                                }
                            ],
                            date: "2019-03-11",
                            dueDate: "2018-12-10",
                            reference: "Website Design",
                            status: xero_node_1.Invoice.StatusEnum.AUTHORISED
                        }
                    ]
                };
                const createInvoiceResponse = yield xero.accountingApi.createInvoices(req.session.activeTenant.tenantId, invoices);
                // AND we'll need a BANK TRANSACTION with OVERPAYMENT
                const newBankTransaction = {
                    type: xero_node_1.BankTransaction.TypeEnum.SPENDOVERPAYMENT,
                    contact: {
                        contactID: getContactsResponse.body.contacts[0].contactID
                    },
                    lineItems: [{ description: "Forgot to cancel the auto payment", lineAmount: 40.0 }],
                    bankAccount: {
                        code: "090"
                    }
                };
                const newBankTransactions = new xero_node_1.BankTransactions();
                newBankTransactions.bankTransactions = [newBankTransaction];
                const newBankTransactionResponse = yield xero.accountingApi.createBankTransactions(req.session.activeTenant.tenantId, newBankTransactions);
                // finally, allocate overpayment to invoice
                const allocation = {
                    amount: 20.50,
                    invoice: {
                        invoiceID: createInvoiceResponse.body.invoices[0].invoiceID
                    },
                    date: "2020-03-13"
                };
                const newAllocations = new xero_node_1.Allocations();
                newAllocations.allocations = [allocation];
                const overpaymentAllocationResponse = yield xero.accountingApi.createOverpaymentAllocations(req.session.activeTenant.tenantId, newBankTransactionResponse.body.bankTransactions[0].overpaymentID, newAllocations);
                // GET ONE
                const getOverpaymentResponse = yield xero.accountingApi.getOverpayment(req.session.activeTenant.tenantId, newBankTransactionResponse.body.bankTransactions[0].overpaymentID);
                res.render("overpayments", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: getOverpaymentsResponse.body.overpayments.length,
                    overpayment: newBankTransactionResponse.body.bankTransactions[0].overpaymentID,
                    allocation: overpaymentAllocationResponse.body.allocations[0].amount,
                    getOne: getOverpaymentResponse.body.overpayments[0].contact.name
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/payments", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                const getPaymentsResponse = yield xero.accountingApi.getPayments(req.session.activeTenant.tenantId);
                // CREATE
                // for that we'll need a contact & invoice
                const getContactsResponse = yield xero.accountingApi.getContacts(req.session.activeTenant.tenantId);
                const invoices = {
                    invoices: [
                        {
                            type: xero_node_1.Invoice.TypeEnum.ACCREC,
                            contact: {
                                contactID: getContactsResponse.body.contacts[0].contactID
                            },
                            lineItems: [
                                {
                                    description: "Acme Tires",
                                    quantity: 2.0,
                                    unitAmount: 20.0,
                                    accountCode: "200",
                                    taxType: "OUTPUT",
                                    lineAmount: 40.0
                                }
                            ],
                            date: "2019-03-11",
                            dueDate: "2018-12-10",
                            reference: "Website Design",
                            status: xero_node_1.Invoice.StatusEnum.AUTHORISED
                        }
                    ]
                };
                const createInvoiceResponse = yield xero.accountingApi.createInvoices(req.session.activeTenant.tenantId, invoices);
                const payments = {
                    payments: [
                        {
                            invoice: {
                                invoiceID: createInvoiceResponse.body.invoices[0].invoiceID
                            },
                            account: {
                                code: "090"
                            },
                            date: "2020-03-12",
                            amount: 3.50
                        },
                    ]
                };
                const createPaymentResponse = yield xero.accountingApi.createPayments(req.session.activeTenant.tenantId, payments);
                // GET ONE
                const getPaymentResponse = yield xero.accountingApi.getPayment(req.session.activeTenant.tenantId, createPaymentResponse.body.payments[0].paymentID);
                // DELETE
                // spec needs to be updated, it's trying to modify a payment but that throws a validation error
                res.render("payments", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: getPaymentsResponse.body.payments.length,
                    newPayment: createPaymentResponse.body.payments[0].paymentID,
                    getPayment: getPaymentResponse.body.payments[0].invoice.contact.name
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/paymentservices", (req, res) => __awaiter(this, void 0, void 0, function* () {
            // must request `paymentservices` scope from api@xero.com
            try {
                //GET ALL
                const getPaymentServicesResponse = yield xero.accountingApi.getPaymentServices(req.session.activeTenant.tenantId);
                // CREATE
                const paymentServices = { paymentServices: [{ paymentServiceName: `PayUpNow ${helper_1.default.getRandomNumber(1000)}`, paymentServiceUrl: "https://www.payupnow.com/?invoiceNo=[INVOICENUMBER]&currency=[CURRENCY]&amount=[AMOUNTDUE]&shortCode=[SHORTCODE]", payNowText: "Time To Pay" }] };
                const createPaymentServiceResponse = yield xero.accountingApi.createPaymentService(req.session.activeTenant.tenantId, paymentServices);
                res.render("paymentservices", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: getPaymentServicesResponse.body.paymentServices.length,
                    create: createPaymentServiceResponse.body.paymentServices[0].paymentServiceID
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/prepayments", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                const getPrepaymentsResponse = yield xero.accountingApi.getPrepayments(req.session.activeTenant.tenantId);
                // CREATE ALLOCATION
                // for that we'll need a contact & invoice
                const getContactsResponse = yield xero.accountingApi.getContacts(req.session.activeTenant.tenantId);
                const invoices = {
                    invoices: [
                        {
                            type: xero_node_1.Invoice.TypeEnum.ACCREC,
                            contact: {
                                contactID: getContactsResponse.body.contacts[0].contactID
                            },
                            lineItems: [
                                {
                                    description: "Acme Tires",
                                    quantity: 2.0,
                                    unitAmount: 20.0,
                                    accountCode: "200",
                                    taxType: "OUTPUT",
                                    lineAmount: 40.0
                                }
                            ],
                            date: "2019-03-11",
                            dueDate: "2018-12-10",
                            reference: "Website Design",
                            status: xero_node_1.Invoice.StatusEnum.AUTHORISED
                        }
                    ]
                };
                const createInvoiceResponse = yield xero.accountingApi.createInvoices(req.session.activeTenant.tenantId, invoices);
                // AND we'll need a BANK TRANSACTION with PREPAYMENT
                const newBankTransaction = {
                    type: xero_node_1.BankTransaction.TypeEnum.RECEIVEPREPAYMENT,
                    contact: {
                        contactID: getContactsResponse.body.contacts[0].contactID
                    },
                    lineItems: [{ description: "Acme Tires", quantity: 2.0, unitAmount: 20.0, accountCode: "200", taxType: "OUTPUT", lineAmount: 40.0 }],
                    bankAccount: {
                        code: "090"
                    }
                };
                const newBankTransactions = new xero_node_1.BankTransactions();
                newBankTransactions.bankTransactions = [newBankTransaction];
                const newBankTransactionResponse = yield xero.accountingApi.createBankTransactions(req.session.activeTenant.tenantId, newBankTransactions);
                // finally, allocate prepayment to invoice
                const allocation = {
                    amount: 20.50,
                    invoice: {
                        invoiceID: createInvoiceResponse.body.invoices[0].invoiceID
                    },
                    date: "1970-01-01"
                };
                const newAllocations = new xero_node_1.Allocations();
                newAllocations.allocations = [allocation];
                const prepaymentAllocationResponse = yield xero.accountingApi.createPrepaymentAllocations(req.session.activeTenant.tenantId, newBankTransactionResponse.body.bankTransactions[0].prepaymentID, newAllocations);
                // CREATE HISTORY
                // "Message": "The document with the supplied id was not found for this endpoint."
                // const historyRecords: HistoryRecords = { historyRecords: [{ details: "Hello World" }] };
                // const prepaymentHistoryResponse = await xero.accountingApi.createPrepaymentHistory(req.session.activeTenant.tenantId, newBankTransactionResponse.body.bankTransactions[0].prepaymentID, historyRecords);
                // GET ONE
                const getPrepaymentResponse = yield xero.accountingApi.getPrepayment(req.session.activeTenant.tenantId, newBankTransactionResponse.body.bankTransactions[0].prepaymentID);
                res.render("prepayments", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: getPrepaymentsResponse.body.prepayments.length,
                    prepayment: newBankTransactionResponse.body.bankTransactions[0].prepaymentID,
                    allocation: prepaymentAllocationResponse.body.allocations[0].amount,
                    remainingCredit: getPrepaymentResponse.body.prepayments[0].remainingCredit
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/purchaseorders", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                const getPurchaseOrdersResponse = yield xero.accountingApi.getPurchaseOrders(req.session.activeTenant.tenantId);
                // CREATE
                // first we need a contactID
                const getContactsResponse = yield xero.accountingApi.getContacts(req.session.activeTenant.tenantId);
                const contactID = getContactsResponse.body.contacts[0].contactID;
                const newPurchaseOrder = {
                    contact: {
                        contactID
                    },
                    date: "2020-02-07",
                    deliveryDate: "2020-02-14",
                    lineAmountTypes: xero_node_1.LineAmountTypes.Exclusive,
                    lineItems: [
                        {
                            description: "Office Chairs",
                            quantity: 5.0000,
                            unitAmount: 120.00
                        }
                    ]
                };
                const purchaseOrders = new xero_node_1.PurchaseOrders();
                purchaseOrders.purchaseOrders = [newPurchaseOrder];
                const createPurchaseOrderResponse = yield xero.accountingApi.createPurchaseOrders(req.session.activeTenant.tenantId, purchaseOrders);
                // GET ONE
                const getPurchaseOrderResponse = yield xero.accountingApi.getPurchaseOrder(req.session.activeTenant.tenantId, createPurchaseOrderResponse.body.purchaseOrders[0].purchaseOrderID);
                // UPDATE
                const updatedPurchaseOrder = newPurchaseOrder;
                updatedPurchaseOrder.deliveryInstructions = "Don't forget the secret knock";
                purchaseOrders.purchaseOrders = [updatedPurchaseOrder];
                const updatePurchaseOrderResponse = yield xero.accountingApi.updatePurchaseOrder(req.session.activeTenant.tenantId, getPurchaseOrderResponse.body.purchaseOrders[0].purchaseOrderID, purchaseOrders);
                res.render("purchaseorders", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: getPurchaseOrdersResponse.body.purchaseOrders.length,
                    create: createPurchaseOrderResponse.body.purchaseOrders[0].purchaseOrderID,
                    get: getPurchaseOrderResponse.body.purchaseOrders[0].lineItems[0].description,
                    update: updatePurchaseOrderResponse.body.purchaseOrders[0].deliveryInstructions
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/purchase-order-as-pdf", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // GET ALL
                const getPurchaseOrdersResponse = yield xero.accountingApi.getPurchaseOrders(req.session.activeTenant.tenantId);
                // GET one as PDF
                const getAsPdf = yield xero.accountingApi.getPurchaseOrderAsPdf(req.session.activeTenant.tenantId, getPurchaseOrdersResponse.body.purchaseOrders[0].purchaseOrderID, { headers: { accept: 'application/pdf' } });
                res.setHeader('Content-Disposition', 'attachment; filename=purchase-order-as-pdf.pdf');
                res.contentType("application/pdf");
                res.send(getAsPdf.body);
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/receipts", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                const getReceiptsResponse = yield xero.accountingApi.getReceipts(req.session.activeTenant.tenantId);
                // first we need a contactID and userID
                const getContactsResponse = yield xero.accountingApi.getContacts(req.session.activeTenant.tenantId);
                const contactID = getContactsResponse.body.contacts[0].contactID;
                const getusersResponse = yield xero.accountingApi.getUsers(req.session.activeTenant.tenantId);
                const userID = getusersResponse.body.users[0].userID;
                // {Receipts}
                const receipts = {
                    receipts: [
                        {
                            contact: {
                                contactID
                            },
                            reference: `Reference ${helper_1.default.getRandomNumber(10000)}`,
                            lineItems: [
                                {
                                    description: "Foobar",
                                    quantity: 2.0,
                                    unitAmount: 20.0,
                                    accountCode: "400",
                                    taxType: "NONE",
                                    lineAmount: 40.0
                                }
                            ],
                            user: {
                                userID
                            },
                            lineAmountTypes: xero_node_1.LineAmountTypes.Inclusive,
                            status: xero_node_1.Receipt.StatusEnum.DRAFT,
                            date: null
                        }
                    ]
                };
                // CREATE
                const createReceiptResponse = yield xero.accountingApi.createReceipt(req.session.activeTenant.tenantId, receipts);
                // GET ONE
                const getReceiptResponse = yield xero.accountingApi.getReceipt(req.session.activeTenant.tenantId, createReceiptResponse.body.receipts[0].receiptID);
                const updatedReceipts = receipts;
                updatedReceipts.receipts[0].lineItems[0].description = 'UPDATED - Foobar';
                // UPDATE
                const updateReceiptResponse = yield xero.accountingApi.updateReceipt(req.session.activeTenant.tenantId, getReceiptResponse.body.receipts[0].receiptID, updatedReceipts);
                res.render("receipts", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: getReceiptsResponse.body.receipts.length,
                    create: createReceiptResponse.body.receipts[0].reference,
                    getOne: getReceiptResponse.body.receipts[0].reference,
                    update: updateReceiptResponse.body.receipts[0].lineItems[0].description
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/reports", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // GET 1099 REPORT
                // optional parameters
                const reportYear = "2019";
                // const getTenNinetyNineResponse = await xero.accountingApi.getReportTenNinetyNine(req.session.activeTenant.tenantId, reportYear);
                // getting a contact first
                const contactsGetResponse = yield xero.accountingApi.getContacts(req.session.activeTenant.tenantId);
                // GET AGED PAYABLES BY CONTACT REPORT
                // required parameters
                const apbcContactID = contactsGetResponse.body.contacts[0].contactID;
                // optional parameters
                const apbcDate = "2019-12-31";
                const apbcFromDate = "2019-01-01";
                const apbcToDate = "2019-12-31";
                const getAgedPayablesByContactResponse = yield xero.accountingApi.getReportAgedPayablesByContact(req.session.activeTenant.tenantId, apbcContactID, apbcDate, apbcFromDate, apbcToDate);
                // GET AGED RECEIVABLES BY CONTACT REPORT
                // required parameters
                const arbcContactID = contactsGetResponse.body.contacts[0].contactID;
                // optional parameters
                const arbcDate = "2019-12-31";
                const arbcFromDate = "2019-01-01";
                const arbcToDate = "2019-12-31";
                const getAgedReceivablesByContactResponse = yield xero.accountingApi.getReportAgedReceivablesByContact(req.session.activeTenant.tenantId, arbcContactID, arbcDate, arbcFromDate, arbcToDate);
                // GET BALANCE SHEET REPORT
                // optional parameters
                const balsheetDate = "2019-04-22";
                const balsheetPeriods = 7;
                const balsheetTimeframe = "QUARTER";
                const balsheetTrackingOptionID1 = undefined;
                const balsheetTrackingOptionID2 = undefined;
                const balsheetStandardLayout = true;
                const balsheetPaymentsOnly = false;
                const getBalanceSheetResponse = yield xero.accountingApi.getReportBalanceSheet(req.session.activeTenant.tenantId, balsheetDate, balsheetPeriods, balsheetTimeframe, balsheetTrackingOptionID1, balsheetTrackingOptionID2, balsheetStandardLayout, balsheetPaymentsOnly);
                // GET BANK SUMMARY REPORT
                // optional parameters
                const banksumFromDate = "2019-01-01";
                const banksumToDate = "2019-12-31";
                const getReportBankSummaryResponse = yield xero.accountingApi.getReportBankSummary(req.session.activeTenant.tenantId, banksumFromDate, banksumToDate);
                // GET BAS REPORT LIST
                const getBASListResponse = yield xero.accountingApi.getReportBASorGSTList(req.session.activeTenant.tenantId);
                // GET BAS REPORT - FOR AUSTRALIA ORGS ONLY, WILL NOT WORK WITH US DEMO COMPANY
                // required parameters
                // const BASReportID: string = "00000000-0000-0000-0000-000000000000";
                // const getBASResponse = await xero.accountingApi.getReportBASorGST(req.session.activeTenant.tenantId, BASReportID);
                // console.log(getBASResponse.body.reports[0] || 'This works for Australia based organisations only');
                // GET BUDGET SUMMARY REPORT
                // optional parameters
                const bsDate = "2019-04-22";
                const bsPeriods = 6;
                const bsTimeframe = 3;
                const getBudgetSummaryResponse = yield xero.accountingApi.getReportBudgetSummary(req.session.activeTenant.tenantId, bsDate, bsPeriods, bsTimeframe);
                // GET EXECUTIVE SUMMARY REPORT
                // optional parameters
                const esDate = "2019-04-22";
                const getExecutiveSummaryResponse = yield xero.accountingApi.getReportExecutiveSummary(req.session.activeTenant.tenantId, esDate);
                // GET GST REPORT LIST
                const getGSTListResponse = yield xero.accountingApi.getReportBASorGSTList(req.session.activeTenant.tenantId);
                // GET GST REPORT - FOR NEW ZEALAND ORGS ONLY, WILL NOT WORK WITH US DEMO COMPANY
                // required parameters
                // const GSTReportID: string = "00000000-0000-0000-0000-000000000000";
                // const getGSTResponse = await xero.accountingApi.getReportBASorGST(req.session.activeTenant.tenantId, GSTReportID);
                // console.log(getGSTResponse.body.reports[0] || 'This works for NEW ZEALAND based organisations only');
                // GET PROFIT AND LOSS REPORT
                // optional parameters
                const plFromDate = "2019-01-01";
                const plToDate = "2019-12-31";
                const plPeriods = 6;
                const plTimeframe = "QUARTER";
                const plTrackingCategoryID = undefined;
                const plTrackingOptionID = undefined;
                const plTrackingCategoryID2 = undefined;
                const plTrackingOptionID2 = undefined;
                const plStandardLayout = true;
                const plPaymentsOnly = false;
                const getProfitAndLossResponse = yield xero.accountingApi.getReportProfitAndLoss(req.session.activeTenant.tenantId, plFromDate, plToDate, plPeriods, plTimeframe, plTrackingCategoryID, plTrackingOptionID, plTrackingCategoryID2, plTrackingOptionID2, plStandardLayout, plPaymentsOnly);
                // GET TRIAL BALANCE REPORT
                // optional parameters
                const tbDate = "2019-04-22";
                const tbPaymentsOnly = false;
                const getTrialBalanceResponse = yield xero.accountingApi.getReportTrialBalance(req.session.activeTenant.tenantId, tbDate, tbPaymentsOnly);
                res.render("reports", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    bankSummaryReportTitle: getReportBankSummaryResponse.body.reports[0].reportTitles.join(' '),
                    tenNinetyNineReportTitle: "`${getTenNinetyNineResponse.body.reports[0].reportName} ${getTenNinetyNineResponse.body.reports[0].reportDate}`",
                    agedPayablesByContactReportTitle: `${getAgedPayablesByContactResponse.body.reports[0].reportName} ${getAgedPayablesByContactResponse.body.reports[0].reportDate}`,
                    agedReceivablesByContactReportTitle: `${getAgedReceivablesByContactResponse.body.reports[0].reportName} ${getAgedReceivablesByContactResponse.body.reports[0].reportDate}`,
                    getBalanceSheetReportTitle: `${getBalanceSheetResponse.body.reports[0].reportName} ${getBalanceSheetResponse.body.reports[0].reportDate}`,
                    getReportBankSummaryReportTitle: `${getReportBankSummaryResponse.body.reports[0].reportName} ${getReportBankSummaryResponse.body.reports[0].reportDate}`,
                    getBudgetSummaryReportTitle: `${getBudgetSummaryResponse.body.reports[0].reportName} ${getBudgetSummaryResponse.body.reports[0].reportDate}`,
                    getExecutiveSummaryReportTitle: `${getExecutiveSummaryResponse.body.reports[0].reportName} ${getExecutiveSummaryResponse.body.reports[0].reportDate}`,
                    getProfitAndLossReportTitle: `${getProfitAndLossResponse.body.reports[0].reportName} ${getProfitAndLossResponse.body.reports[0].reportDate}`,
                    getTrialBalanceReportTitle: `${getTrialBalanceResponse.body.reports[0].reportName} ${getTrialBalanceResponse.body.reports[0].reportDate}`
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/taxrates", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                const getAllResponse = yield xero.accountingApi.getTaxRates(req.session.activeTenant.tenantId);
                const newTaxRate = {
                    name: `Tax Rate Name ${helper_1.default.getRandomNumber(1000000)}`,
                    reportTaxType: undefined,
                    taxType: 'INPUT',
                    taxComponents: [
                        {
                            name: "State Tax",
                            rate: 7.5,
                            isCompound: false,
                            isNonRecoverable: false
                        },
                        {
                            name: "Local Sales Tax",
                            rate: 0.625,
                            isCompound: false,
                            isNonRecoverable: false
                        }
                    ]
                };
                const taxRates = new xero_node_1.TaxRates();
                taxRates.taxRates = [newTaxRate];
                // CREATE
                const createResponse = yield xero.accountingApi.createTaxRates(req.session.activeTenant.tenantId, taxRates);
                res.render("taxrates", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: getAllResponse.body.taxRates.length,
                    created: createResponse.body.taxRates[0].name
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/trackingcategories", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // GET ALL
                const getAllResponse = yield xero.accountingApi.getTrackingCategories(req.session.activeTenant.tenantId);
                // New Tracking Category
                const trackingCategory = {
                    name: `Tracking Category ${helper_1.default.getRandomNumber(1000000)}`,
                    status: xero_node_1.TrackingCategory.StatusEnum.ACTIVE
                };
                // New Tracking Category Option
                const trackingCategoryOption = {
                    name: `Tracking Option ${helper_1.default.getRandomNumber(1000000)}`,
                    status: xero_node_1.TrackingOption.StatusEnum.ACTIVE
                };
                // CREATE
                const createCategoryResponse = yield xero.accountingApi.createTrackingCategory(req.session.activeTenant.tenantId, trackingCategory);
                yield xero.accountingApi.createTrackingOptions(req.session.activeTenant.tenantId, createCategoryResponse.body.trackingCategories[0].trackingCategoryID, trackingCategoryOption);
                // GET ONE
                const getOneResponse = yield xero.accountingApi.getTrackingCategory(req.session.activeTenant.tenantId, createCategoryResponse.body.trackingCategories[0].trackingCategoryID);
                // UPDATE
                const updateResponse = yield xero.accountingApi.updateTrackingCategory(req.session.activeTenant.tenantId, getOneResponse.body.trackingCategories[0].trackingCategoryID, { name: `${getOneResponse.body.trackingCategories[0].name} - updated` });
                // DELETE
                const deleteResponse = yield xero.accountingApi.deleteTrackingCategory(req.session.activeTenant.tenantId, createCategoryResponse.body.trackingCategories[0].trackingCategoryID);
                res.render("trackingcategories", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: getAllResponse.body.trackingCategories.length,
                    created: createCategoryResponse.body.trackingCategories[0].trackingCategoryID,
                    got: getOneResponse.body.trackingCategories[0].name,
                    updated: updateResponse.body.trackingCategories[0].name,
                    deleted: deleteResponse.body.trackingCategories[0].status
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/users", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                const getAllUsers = yield xero.accountingApi.getUsers(req.session.activeTenant.tenantId);
                // GET ONE USER
                const getUser = yield xero.accountingApi.getUser(req.session.activeTenant.tenantId, getAllUsers.body.users[0].userID);
                res.render("users", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    user: getUser.body.users[0].emailAddress,
                    count: getAllUsers.body.users.length
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/quotes", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                const getAllQuotes = yield xero.accountingApi.getQuotes(req.session.activeTenant.tenantId);
                // CREATE QUOTE
                const contactsResponse = yield xero.accountingApi.getContacts(req.session.activeTenant.tenantId);
                const useContact = { contactID: contactsResponse.body.contacts[0].contactID };
                // CREATE QUOTES
                const quote = {
                    date: '2020-02-05',
                    quoteNumber: "QuoteNum:" + helper_1.default.getRandomNumber(1000000),
                    contact: useContact,
                    lineItems: [
                        {
                            description: "Consulting services",
                            taxType: "OUTPUT",
                            quantity: 20,
                            unitAmount: 100.00,
                            accountCode: "200"
                        }
                    ]
                };
                const quotes = {
                    quotes: [
                        quote
                    ]
                };
                const createQuotes = yield xero.accountingApi.updateOrCreateQuotes(req.session.activeTenant.tenantId, quotes);
                const quoteId = createQuotes.body.quotes[0].quoteID;
                const filename = "xero-dev.png";
                const pathToUpload = path.resolve(__dirname, "../public/images/xero-dev.png");
                const readStream = fs.createReadStream(pathToUpload);
                const contentType = mime.lookup(filename);
                const addQuoteAttachment = yield xero.accountingApi.createQuoteAttachmentByFileName(req.session.activeTenant.tenantId, quoteId, filename, readStream, {
                    headers: {
                        'Content-Type': contentType
                    }
                });
                // GET ONE
                const getOneQuote = yield xero.accountingApi.getQuote(req.session.activeTenant.tenantId, getAllQuotes.body.quotes[0].quoteID);
                res.render("quotes", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: getAllQuotes.body.quotes.length,
                    getOneQuoteNumber: getOneQuote.body.quotes[0].quoteNumber,
                    createdQuotesId: quoteId,
                    addQuoteAttachment: addQuoteAttachment.body
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        // ******************************************************************************************************************** ASSETS API
        router.get("/assets", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // GET ASSET SETTINGS
                const getAssetSettings = yield xero.assetApi.getAssetSettings(req.session.activeTenant.tenantId);
                // GET ASSETTYPES
                const getAssetTypes = yield xero.assetApi.getAssetTypes(req.session.activeTenant.tenantId);
                // CREATE ASSET
                const asset = {
                    assetName: `AssetName: ${helper_1.default.getRandomNumber(1000000)}`,
                    assetNumber: `Asset: ${helper_1.default.getRandomNumber(1000000)}`,
                    assetStatus: models_1.AssetStatus.Draft
                };
                const createAsset = yield xero.assetApi.createAsset(req.session.activeTenant.tenantId, asset);
                // GET ASSET
                const getAsset = yield xero.assetApi.getAssetById(req.session.activeTenant.tenantId, createAsset.body.assetId);
                // GET ASSETS
                const getAssets = yield xero.assetApi.getAssets(req.session.activeTenant.tenantId, models_1.AssetStatusQueryParam.DRAFT);
                res.render("assets", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    assetSettings: getAssetSettings.body,
                    assetTypes: getAssetTypes.body,
                    getAsset: getAsset.body.assetName,
                    createAsset: createAsset.body.assetNumber,
                    assets: getAssets.body.items
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        // ******************************************************************************************************************** PROJECTS API
        router.get("/projects", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                const getAllResponse = yield xero.projectApi.getProjects(req.session.activeTenant.tenantId);
                //GET MULTIPLE SPECIFIED
                const getMultipleSpecifiedResponse = yield xero.projectApi.getProjects(req.session.activeTenant.tenantId, [getAllResponse.body.items[0].projectId, getAllResponse.body.items[1].projectId]);
                // CREATE
                // we'll need a contact first
                const contactsResponse = yield xero.accountingApi.getContacts(req.session.activeTenant.tenantId);
                const newProject = {
                    contactId: contactsResponse.body.contacts[0].contactID,
                    name: 'New Project ' + helper_1.default.getRandomNumber(1000),
                    deadlineUtc: new Date(),
                    estimateAmount: 3.50
                };
                const createResponse = yield xero.projectApi.createProject(req.session.activeTenant.tenantId, newProject);
                // Projects API DB transaction intermittently needs a few seconds to persist record in the database
                yield sleep(3000);
                // GET ONE
                const getResponse = yield xero.projectApi.getProject(req.session.activeTenant.tenantId, createResponse.body.projectId);
                // UPDATE
                const updateProject = {
                    name: createResponse.body.name,
                    deadlineUtc: createResponse.body.deadlineUtc,
                    estimateAmount: 350.00
                };
                const updateResponse = yield xero.projectApi.updateProject(req.session.activeTenant.tenantId, createResponse.body.projectId, updateProject);
                // PATCH
                const patch = {
                    status: models_2.ProjectStatus.CLOSED
                };
                const patchResponse = yield xero.projectApi.patchProject(req.session.activeTenant.tenantId, createResponse.body.projectId, patch);
                res.render("projects", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: getAllResponse.body.pagination.itemCount,
                    create: createResponse.body.projectId,
                    get: getResponse.body.name,
                    update: updateResponse.response.statusCode,
                    patch: patchResponse.response.statusCode
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/project-users", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // GET PROJECT USERS
                const getProjectUsersResponse = yield xero.projectApi.getProjectUsers(req.session.activeTenant.tenantId);
                res.render("project-users", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    users: getProjectUsersResponse.body.items,
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/tasks", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                // we'll need a projectID
                const projectsResponse = yield xero.projectApi.getProjects(req.session.activeTenant.tenantId);
                const getTasksResponse = yield xero.projectApi.getTasks(req.session.activeTenant.tenantId, projectsResponse.body.items[0].projectId);
                // CREATE
                // GET ONE
                const getTaskResponse = yield xero.projectApi.getTask(req.session.activeTenant.tenantId, projectsResponse.body.items[0].projectId, getTasksResponse.body.items[0].taskId);
                // UPDATE
                res.render("tasks", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: getTasksResponse.body.pagination.itemCount,
                    get: getTaskResponse.body.name
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/time", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                //GET ALL
                // we'll need a projectID
                const projectsResponse = yield xero.projectApi.getProjects(req.session.activeTenant.tenantId);
                const getTimeEntriesResponse = yield xero.projectApi.getTimeEntries(req.session.activeTenant.tenantId, projectsResponse.body.items[0].projectId);
                // CREATE
                const timeEntry = {
                    userId: getTimeEntriesResponse.body.items[0].userId,
                    taskId: getTimeEntriesResponse.body.items[0].taskId,
                    dateUtc: new Date(),
                    duration: 10000,
                    description: "time it takes to become an expert"
                };
                const createTimeEntryResponse = yield xero.projectApi.createTimeEntry(req.session.activeTenant.tenantId, projectsResponse.body.items[0].projectId, timeEntry);
                yield sleep(3000);
                // GET ONE
                const getTimeEntryResponse = yield xero.projectApi.getTimeEntry(req.session.activeTenant.tenantId, projectsResponse.body.items[0].projectId, createTimeEntryResponse.body.timeEntryId);
                // UPDATE
                timeEntry.description = "time it takes to become an expert - edited";
                const updateTimeEntryResponse = yield xero.projectApi.updateTimeEntry(req.session.activeTenant.tenantId, projectsResponse.body.items[0].projectId, createTimeEntryResponse.body.timeEntryId, timeEntry);
                // DELETE
                const deleteTimeEntryResponse = yield xero.projectApi.deleteTimeEntry(req.session.activeTenant.tenantId, projectsResponse.body.items[0].projectId, createTimeEntryResponse.body.timeEntryId);
                res.render("time", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: getTimeEntriesResponse.body.pagination.itemCount,
                    create: createTimeEntryResponse.body.timeEntryId,
                    get: getTimeEntryResponse.body.description,
                    update: updateTimeEntryResponse.response.statusCode,
                    deleted: deleteTimeEntryResponse.response.statusCode
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        // ******************************************************************************************************************** payroll-au
        router.get("/payroll-au-employees", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // since we already have an Employee model in the Accounting API scope, we've imported and renamed like so:
                // import { Employee as AUPayrollEmployee } from 'xero-node/dist/gen/model/payroll-au/models';
                const homeAddress = {
                    addressLine1: "1",
                    city: "Island Bay",
                    region: models_3.State.QLD,
                    postalCode: "6023",
                    country: "AUSTRALIA"
                };
                const employee = {
                    firstName: 'Charlie',
                    lastName: 'Chaplin',
                    dateOfBirth: xero.formatMsDate("1990-02-05"),
                    homeAddress: homeAddress
                };
                const createEmployee = yield xero.payrollAUApi.createEmployee(req.session.activeTenant.tenantId, [employee]);
                const getEmployees = yield xero.payrollAUApi.getEmployees(req.session.activeTenant.tenantId);
                const updatedEmployee = employee;
                updatedEmployee.firstName = 'Chuck';
                const updateEmployee = yield xero.payrollAUApi.updateEmployee(req.session.activeTenant.tenantId, getEmployees.body.employees[0].employeeID, [updatedEmployee]);
                res.render("payroll-au-employee", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    getEmployees: getEmployees.body.employees,
                    createdEmployee: createEmployee.body.employees[0],
                    updateEmployee: updateEmployee.body.employees[0]
                });
            }
            catch (e) {
                console.log('Are you using an Australia Org with the Payroll settings completed? (https://payroll.xero.com/Dashboard/Details)');
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/leave-application", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const leaveItems = yield xero.payrollAUApi.getLeaveApplications(req.session.activeTenant.tenantId);
                // xero.payrollAUApi.createLeaveApplication
                // xero.payrollAUApi.getLeaveApplication
                // xero.payrollAUApi.updateLeaveApplication
                res.render("leave-application", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    leaveItems: leaveItems.body.leaveApplications
                });
            }
            catch (e) {
                console.log('Are you using an Australia Org with the Payroll settings completed? (https://payroll.xero.com/Dashboard/Details)');
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/pay-item", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const payItems = yield xero.payrollAUApi.getPayItems(req.session.activeTenant.tenantId);
                // xero.payrollAUApi.createPayItem
                res.render("pay-item", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    payItems: payItems.body.payItems
                });
            }
            catch (e) {
                console.log('Are you using an Australia Org with the Payroll settings completed? (https://payroll.xero.com/Dashboard/Details)');
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/pay-run", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const payRuns = yield xero.payrollAUApi.getPayRuns(req.session.activeTenant.tenantId);
                // xero.payrollAUApi.createPayRun
                // xero.payrollAUApi.getPayRun
                // xero.payrollAUApi.updatePayRun
                res.render("pay-run", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    payRuns: payRuns.body.payRuns
                });
            }
            catch (e) {
                console.log('Are you using an Australia Org with the Payroll settings completed? (https://payroll.xero.com/Dashboard/Details)');
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/payroll-calendar", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // xero.payrollAUApi.createPayrollCalendar
                // xero.payrollAUApi.getPayrollCalendar
                const getPayrollCalendars = yield xero.payrollAUApi.getPayrollCalendars(req.session.activeTenant.tenantId);
                res.render("payroll-calendar", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    getPayrollCalendars: getPayrollCalendars.body.payrollCalendars
                });
            }
            catch (e) {
                console.log('Are you using an Australia Org with the Payroll settings completed? (https://payroll.xero.com/Dashboard/Details)');
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/superfund", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const getSuperfunds = yield xero.payrollAUApi.getSuperfunds(req.session.activeTenant.tenantId);
                // xero.payrollAUApi.getSuperfund
                // xero.payrollAUApi.createSuperfund
                // xero.payrollAUApi.getSuperfundProducts
                // xero.payrollAUApi.updateSuperfund
                res.render("superfund", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    getSuperFunds: getSuperfunds.body.superFunds
                });
            }
            catch (e) {
                console.log('Are you using an Australia Org with the Payroll settings completed? (https://payroll.xero.com/Dashboard/Details)');
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/timesheet", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // xero.payrollAUApi.getTimesheets
                const response = yield xero.payrollAUApi.getTimesheets(req.session.activeTenant.tenantId);
                // xero.payrollAUApi.createTimesheet
                // xero.payrollAUApi.getTimesheet
                // xero.payrollAUApi.updateTimesheet
                res.render("timesheet", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    timeSheets: response.body.timesheets
                });
            }
            catch (e) {
                console.log('Are you using an Australia Org with the Payroll settings completed? (https://payroll.xero.com/Dashboard/Details)');
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/payslip", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // xero.payrollAUApi.getPayslip
                // xero.payrollAUApi.updatePayslipByID
                res.render("payslip", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                });
            }
            catch (e) {
                console.log('Are you using an Australia Org with the Payroll settings completed? (https://payroll.xero.com/Dashboard/Details)');
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/payroll-au-settings", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const getPayrollSettingsResponse = yield xero.payrollAUApi.getSettings(req.session.activeTenant.tenantId);
                res.render("payroll-au-settings", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    payrollSettings: getPayrollSettingsResponse.body
                });
            }
            catch (e) {
                console.log('Are you using an Australia Org with the Payroll settings completed? (https://payroll.xero.com/Dashboard/Details)');
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        // ******************************************************************************************************************** BANKFEEDS API
        router.get("/bankfeed-connections", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const getBankfeedsResponse = yield xero.bankFeedsApi.getFeedConnections(req.session.activeTenant.tenantId);
                const feedConnections = {
                    items: [
                        {
                            accountToken: `10000${helper_1.default.getRandomNumber(999)}`,
                            accountNumber: `${helper_1.default.getRandomNumber(10000)}`,
                            accountName: `Account ${helper_1.default.getRandomNumber(1000)}`,
                            accountType: models_4.FeedConnection.AccountTypeEnum.BANK,
                            currency: models_4.CurrencyCode.USD,
                            country: models_4.CountryCode.US,
                        }
                    ]
                };
                const createBankfeedResponse = yield xero.bankFeedsApi.createFeedConnections(req.session.activeTenant.tenantId, feedConnections);
                // DB needs a bit of time to persist creation
                yield sleep(3000);
                const getBankfeedResponse = yield xero.bankFeedsApi.getFeedConnection(req.session.activeTenant.tenantId, createBankfeedResponse.body.items[0].id);
                const deleteConnection = {
                    items: [
                        {
                            id: getBankfeedResponse.body.id
                        }
                    ]
                };
                const deleteBankfeedResponse = yield xero.bankFeedsApi.deleteFeedConnections(req.session.activeTenant.tenantId, deleteConnection);
                res.render("bankfeed-connections", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    bankfeeds: getBankfeedsResponse.body.items.length,
                    created: createBankfeedResponse.body.items[0].id,
                    get: getBankfeedResponse.body.accountName,
                    deleted: deleteBankfeedResponse.response.statusCode
                });
            }
            catch (e) {
                console.log('Do you have XeroAPI permissions to work with this endpoint? (https://developer.xero.com/documentation/bank-feeds-api/overview)');
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/bankfeed-statements", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const getStatementsResponse = yield xero.bankFeedsApi.getStatements(req.session.activeTenant.tenantId);
                // we're going to need a feed connection first
                const feedConnections = {
                    items: [
                        {
                            accountToken: `10000${helper_1.default.getRandomNumber(999)}`,
                            accountNumber: `${helper_1.default.getRandomNumber(10000)}`,
                            accountName: `Account ${helper_1.default.getRandomNumber(1000)}`,
                            accountType: models_4.FeedConnection.AccountTypeEnum.BANK,
                            country: models_4.CountryCode.US,
                            currency: models_4.CurrencyCode.USD
                        }
                    ]
                };
                const createBankfeedResponse = yield xero.bankFeedsApi.createFeedConnections(req.session.activeTenant.tenantId, feedConnections);
                yield sleep(3000);
                const statements = {
                    items: [
                        {
                            feedConnectionId: createBankfeedResponse.body.items[0].id,
                            startDate: "2020-05-06",
                            endDate: "2020-05-07",
                            startBalance: {
                                amount: 100,
                                creditDebitIndicator: models_4.CreditDebitIndicator.DEBIT
                            },
                            endBalance: {
                                amount: 90,
                                creditDebitIndicator: models_4.CreditDebitIndicator.DEBIT
                            },
                            statementLines: [
                                {
                                    postedDate: "2020-05-06",
                                    description: "Description for statement line 1",
                                    amount: 5,
                                    creditDebitIndicator: models_4.CreditDebitIndicator.CREDIT,
                                    transactionId: "transaction-id-1",
                                },
                                {
                                    postedDate: "2020-05-06",
                                    description: "Description for statement line 2",
                                    amount: 5,
                                    creditDebitIndicator: models_4.CreditDebitIndicator.CREDIT,
                                    transactionId: "transaction-id-2",
                                }
                            ]
                        }
                    ]
                };
                const createStatementResponse = yield xero.bankFeedsApi.createStatements(req.session.activeTenant.tenantId, statements);
                const getStatementResponse = yield xero.bankFeedsApi.getStatement(req.session.activeTenant.tenantId, createStatementResponse.body.items[0].id);
                res.render("bankfeed-statements", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    count: getStatementsResponse.body.items.length,
                    created: createStatementResponse.body.items[0].id,
                    get: getStatementResponse.body
                });
            }
            catch (e) {
                console.log('Do you have XeroAPI permissions to work with this endpoint? (https://developer.xero.com/documentation/bank-feeds-api/overview)');
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        // ******************************************************************************************************************** payroll-uk
        router.get("/payroll-uk-employees", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const getEmployeesResponse = yield xero.payrollUKApi.getEmployees(req.session.activeTenant.tenantId);
                const employee = {
                    title: "Mr",
                    firstName: "Edgar",
                    lastName: "Allan Po",
                    dateOfBirth: "1985-03-24",
                    gender: models_5.Employee.GenderEnum.M,
                    email: "tester@gmail.com",
                    phoneNumber: "0400123456",
                    address: {
                        "addressLine1": "171 Midsummer",
                        "city": "Milton Keyness",
                        "postCode": "MK9 1EB"
                    }
                };
                const createEmployeeResponse = yield xero.payrollUKApi.createEmployee(req.session.activeTenant.tenantId, employee);
                const getEmployeeResponse = yield xero.payrollUKApi.getEmployee(req.session.activeTenant.tenantId, createEmployeeResponse.body.employee.employeeID);
                const updatedEmployee = employee;
                updatedEmployee.email = 'thetelltaleheart@gmail.com';
                const updateEmployeeResponse = yield xero.payrollUKApi.updateEmployee(req.session.activeTenant.tenantId, createEmployeeResponse.body.employee.employeeID, updatedEmployee);
                res.render("payroll-uk-employees", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    employees: getEmployeesResponse.body.employees,
                    created: createEmployeeResponse.body.employee,
                    got: getEmployeeResponse.body.employee,
                    updated: updateEmployeeResponse.body.employee
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/employment", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // you'll need an employeeID, NICategory, and Payroll Calendar ID
                // const employment: Employment = {
                //   startDate,
                //   payrollCalendarID,
                //   niCategory,
                //   employeeNumber
                // };
                // const createEmploymentResponse = await xero.payrollUKApi.createEmployment(req.session.activeTenant.tenantId, employeeID, employment);
                res.render("employment", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/employees-tax", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const getEmployeesResponse = yield xero.payrollUKApi.getEmployees(req.session.activeTenant.tenantId);
                const getEmployeeTaxResponse = yield xero.payrollUKApi.getEmployeeTax(req.session.activeTenant.tenantId, getEmployeesResponse.body.employees[0].employeeID);
                res.render("employees-tax", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    employeeTax: getEmployeeTaxResponse.body.employeeTax
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/employee-opening-balances", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // const getEmployeesResponse = await xero.payrollUKApi.getEmployees(req.session.activeTenant.tenantId);
                // const response = await xero.payrollUKApi.getEmployeeOpeningBalances(req.session.activeTenant.tenantId, getEmployeesResponse.body.employees[0].employeeID);
                // xero.payrollUKApi.createEmployeeOpeningBalances
                // xero.payrollUKApi.updateEmployeeOpeningBalances
                res.render("employee-opening-balances", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/employees-leave", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const getEmployeesResponse = yield xero.payrollUKApi.getEmployees(req.session.activeTenant.tenantId);
                const response = yield xero.payrollUKApi.getEmployeeLeaves(req.session.activeTenant.tenantId, getEmployeesResponse.body.employees[0].employeeID);
                // xero.payrollUKApi.createEmployeeLeave
                // xero.payrollUKApi.getEmployeeLeave
                // xero.payrollUKApi.updateEmployeeLeave
                // xero.payrollUKApi.deleteEmployeeLeave
                res.render("employees-leave", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    leaves: response.body.leave
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/employees-leave-balances", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const getEmployeesResponse = yield xero.payrollUKApi.getEmployees(req.session.activeTenant.tenantId);
                const response = yield xero.payrollUKApi.getEmployeeLeaveBalances(req.session.activeTenant.tenantId, getEmployeesResponse.body.employees[0].employeeID);
                res.render("employees-leave-balances", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    leaveBalances: response.body.leaveBalances
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/employees-statutory-leave-balances", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const getEmployeesResponse = yield xero.payrollUKApi.getEmployees(req.session.activeTenant.tenantId);
                const response = yield xero.payrollUKApi.getEmployeeStatutoryLeaveBalances(req.session.activeTenant.tenantId, getEmployeesResponse.body.employees[0].employeeID);
                res.render("employees-statutory-leave-balances", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    leaveBalance: response.body.leaveBalance
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/employees-statutory-leave-summary", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const getEmployeesResponse = yield xero.payrollUKApi.getEmployees(req.session.activeTenant.tenantId);
                const response = yield xero.payrollUKApi.getStatutoryLeaveSummary(req.session.activeTenant.tenantId, getEmployeesResponse.body.employees[0].employeeID);
                res.render("employees-statutory-leave-summary", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    leaveSummary: response.body
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/employees-statutory-sick-leave", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                // xero.payrollUKApi.getEmployeeStatutorySickLeave
                // xero.payrollUKApi.createEmployeeStatutorySickLeave
                res.render("employees-statutory-sick-leave", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/employees-leave-periods", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const getEmployeesResponse = yield xero.payrollUKApi.getEmployees(req.session.activeTenant.tenantId);
                const response = yield xero.payrollUKApi.getEmployeeLeavePeriods(req.session.activeTenant.tenantId, getEmployeesResponse.body.employees[0].employeeID, "2018-06-15", "2020-06-15");
                res.render("employees-leave-periods", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    leavePeriods: response.body.periods
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/employees-leave-types", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const getEmployeesResponse = yield xero.payrollUKApi.getEmployees(req.session.activeTenant.tenantId);
                const response = yield xero.payrollUKApi.getEmployeeLeaveTypes(req.session.activeTenant.tenantId, getEmployeesResponse.body.employees[0].employeeID);
                // xero.payrollUKApi.createEmployeeLeaveType
                res.render("employees-leave-types", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    leaveTypes: response.body.leaveTypes
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/employees-pay-templates", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const getEmployeesResponse = yield xero.payrollUKApi.getEmployees(req.session.activeTenant.tenantId);
                const response = yield xero.payrollUKApi.getEmployeePayTemplate(req.session.activeTenant.tenantId, getEmployeesResponse.body.employees[0].employeeID);
                // xero.payrollUKApi.createEmployeeEarningsTemplate
                // xero.payrollUKApi.updateEmployeeEarningsTemplate
                // xero.payrollUKApi.createMultipleEmployeeEarningsTemplate
                // xero.payrollUKApi.deleteEmployeeEarningsTemplate
                res.render("employees-pay-templates", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    payTemplate: response.body.payTemplate
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/employer-pensions", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield xero.payrollUKApi.getBenefits(req.session.activeTenant.tenantId);
                // xero.payrollUKApi.createBenefit
                // xero.payrollUKApi.getBenefit
                res.render("employer-pensions", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    benefits: response.body.benefits
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/deductions", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield xero.payrollUKApi.getDeductions(req.session.activeTenant.tenantId);
                // xero.payrollUKApi.createDeduction
                // xero.payrollUKApi.getDeduction
                res.render("deductions", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    deductions: response.body.deductions
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/earnings-orders", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield xero.payrollUKApi.getEarningsOrders(req.session.activeTenant.tenantId);
                // xero.payrollUKApi.getEarningsOrder
                res.render("earnings-orders", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    deductions: response.body.statutoryDeductions
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/earnings-rates", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield xero.payrollUKApi.getEarningsRates(req.session.activeTenant.tenantId);
                // xero.payrollUKApi.createEarningsRate
                // xero.payrollUKApi.getEarningsRate
                res.render("earnings-rates", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    rates: response.body.earningsRates
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/leave-types", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield xero.payrollUKApi.getLeaveTypes(req.session.activeTenant.tenantId);
                // xero.payrollUKApi.getLeaveType
                // xero.payrollUKApi.createLeaveType
                res.render("leave-types", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    types: response.body.leaveTypes
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/reimbursements", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield xero.payrollUKApi.getReimbursements(req.session.activeTenant.tenantId);
                // xero.payrollUKApi.getReimbursement
                // xero.payrollUKApi.createReimbursement
                res.render("reimbursements", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    reimbursements: response.body.reimbursements
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/timesheets", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield xero.payrollUKApi.getTimesheets(req.session.activeTenant.tenantId);
                // xero.payrollUKApi.getTimesheet
                // xero.payrollUKApi.createTimesheet
                // xero.payrollUKApi.createTimesheetLine
                // xero.payrollUKApi.updateTimesheetLine
                // xero.payrollUKApi.approveTimesheet
                // xero.payrollUKApi.revertTimesheet
                // xero.payrollUKApi.deleteTimesheet
                // xero.payrollUKApi.deleteTimesheetLine
                res.render("timesheets", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    timesheets: response.body.timesheets
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/payment-methods", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const getEmployeesResponse = yield xero.payrollUKApi.getEmployees(req.session.activeTenant.tenantId);
                const response = yield xero.payrollUKApi.getEmployeePaymentMethod(req.session.activeTenant.tenantId, getEmployeesResponse.body.employees[0].employeeID);
                // xero.payrollUKApi.createEmployeePaymentMethod
                res.render("payment-methods", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    paymentMethod: response.body.paymentMethod
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/pay-run-calendars", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield xero.payrollUKApi.getPayRunCalendars(req.session.activeTenant.tenantId);
                // xero.payrollUKApi.getPayRunCalendar
                // xero.payrollUKApi.createPayRunCalendar
                res.render("pay-run-calendars", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    payRunCalendars: response.body.payRunCalendars
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/salary-wages", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const getEmployeesResponse = yield xero.payrollUKApi.getEmployees(req.session.activeTenant.tenantId);
                const response = yield xero.payrollUKApi.getEmployeeSalaryAndWages(req.session.activeTenant.tenantId, getEmployeesResponse.body.employees[0].employeeID);
                // xero.payrollUKApi.getEmployeeSalaryAndWage
                // xero.payrollUKApi.createEmployeeSalaryAndWage
                // xero.payrollUKApi.updateEmployeeSalaryAndWage
                // xero.payrollUKApi.deleteEmployeeSalaryAndWage
                res.render("salary-wages", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    salaryAndWages: response.body.salaryAndWages
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/pay-runs", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield xero.payrollUKApi.getPayRuns(req.session.activeTenant.tenantId);
                // xero.payrollUKApi.getPayRun
                // xero.payrollUKApi.updatePayRun
                res.render("pay-runs", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    payRuns: response.body.payRuns
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/payslips", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const getPayRunsResponse = yield xero.payrollUKApi.getPayRuns(req.session.activeTenant.tenantId);
                const response = yield xero.payrollUKApi.getPayslips(req.session.activeTenant.tenantId, getPayRunsResponse.body.payRuns[0].payRunID);
                // xero.payrollUKApi.getPaySlip
                res.render("payslips", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    paySlips: response.body.paySlips
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/settings", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield xero.payrollUKApi.getSettings(req.session.activeTenant.tenantId);
                res.render("settings", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    settings: response.body.settings
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        router.get("/tracking-categories", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield xero.payrollUKApi.getTrackingCategories(req.session.activeTenant.tenantId);
                res.render("tracking-categories", {
                    consentUrl: yield xero.buildConsentUrl(),
                    authenticated: this.authenticationData(req, res),
                    trackingCategories: response.body.trackingCategories
                });
            }
            catch (e) {
                res.status(res.statusCode);
                res.render("shared/error", {
                    consentUrl: yield xero.buildConsentUrl(),
                    error: e
                });
            }
        }));
        const fileStoreOptions = {};
        this.app.use(session({
            secret: "something crazy",
            store: new FileStore(fileStoreOptions),
            resave: false,
            saveUninitialized: false,
            cookie: { secure: false },
        }));
        this.app.use("/", router);
    }
}
exports.default = new App().app;
//# sourceMappingURL=app.js.map