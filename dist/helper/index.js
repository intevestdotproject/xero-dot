"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class helper {
    static getRandomNumber(range) {
        return Math.round(Math.random() * ((range || 100) - 1) + 1);
    }
}
exports.default = helper;
//# sourceMappingURL=index.js.map